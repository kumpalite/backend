/* initialize database */

CREATE TABLE IF NOT EXISTS "public"."Article" (
    id UUID PRIMARY KEY NOT NULL,
    slug STRING UNIQUE,
    title STRING,
    content STRING,
    status STRING,
    user_id UUID,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    categories STRING[],
    thumbnail STRING
);

CREATE TABLE IF NOT EXISTS "public"."Category" (
    user_id UUID,
    name STRING,
    slug STRING UNIQUE
);

CREATE INDEX IF NOT EXISTS article_user_id_idx ON articles(user_id);
CREATE INDEX IF NOT EXISTS article_status_idx ON articles(status);

