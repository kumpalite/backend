/* initialize database */
CREATE DATABASE commentdb;

CREATE TABLE IF NOT EXISTS "public"."Comment" (
    id UUID,
    content STRING,
    article_id UUID,
    user_id UUID,
    created_at TIMESTAMP
);

create index idx_article_id on public."Comment" (article_id)

