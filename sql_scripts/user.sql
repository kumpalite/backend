/* initialize database */
CREATE DATABASE userdb;

/* use this in userdb */ 
CREATE TABLE IF NOT EXISTS "public"."User" (
    id UUID,
    fullname string,
    username string,
    password string,
    email string
);
