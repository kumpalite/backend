package server

import (
	"comment_service/config"
	pb "comment_service/proto"
	"comment_service/service"
	"context"
	"net"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

type CommentServiceServer struct {
	service *service.CommentService
}

func NewServer(config config.Config) pb.CommentServiceServer {
	service := service.NewCommentService(config)
	return &CommentServiceServer{
		service: service,
	}
}

func (server *CommentServiceServer) CreateComment(ctx context.Context, param *pb.CommentRequest) (*pb.CommentResponse, error) {
	comment, err := server.service.CreateComment(param.IdUser, param.IdArticle, param.Content)
	if err != nil {
		return nil, err
	}

	res := &pb.CommentResponse{
		Id:        comment.ID.String(),
		IdUser:    comment.UserID.String(),
		IdArticle: comment.ArticleID.String(),
		Content:   comment.Content,
	}
	return res, nil
}

func (server *CommentServiceServer) GetCommentByArticleId(ctx context.Context, param *pb.CommentRequest) (*pb.CommentListResponse, error) {
	commentsFromService, err := server.service.FindCommentsByIdArticle(param.IdArticle, param.Limit, param.Offset)
	if err != nil {
		return nil, err
	}

	var comments []*pb.CommentResponse
	for _, v := range commentsFromService {
		a := &pb.CommentResponse{
			Id:        v.ID.String(),
			IdUser:    v.UserID.String(),
			IdArticle: v.ArticleID.String(),
			Content:   v.Content,
		}
		comments = append(comments, a)
	}

	res := &pb.CommentListResponse{
		CommentList: comments,
	}
	return res, nil
}

func (server *CommentServiceServer) CountAllCommentByArticleId(ctx context.Context, param *pb.CommentRequest) (*pb.CountCommentResponse, error) {
	value, err := server.service.CountCommentsByArticleId(param.IdArticle)
	if err != nil {
		return nil, err
	}

	return &pb.CountCommentResponse{
		Total: value,
	}, nil
}

func Run(config config.Config) {
	listen, err := net.Listen("tcp", config.PORT)
	if err != nil {
		log.Fatalf("ERROR %s", err)
	}

	server := grpc.NewServer()
	reflection.Register(server)
	commentServer := NewServer(config)
	pb.RegisterCommentServiceServer(server, commentServer)

	log.Println("server is on ", config.PORT)

	log.Fatalf("err: %s", server.Serve(listen))
}
