package service

import (
	"comment_service/config"
	db "comment_service/database"
	"comment_service/domain"
	"comment_service/repository"
	"comment_service/repository/database"

	"github.com/labstack/gommon/log"
	"github.com/satori/uuid"
	"github.com/sirupsen/logrus"
)

// CommentService is
type CommentService struct {
	Query      repository.CommentQuery
	Repository repository.CommentRepository
}

// NewCommentService is to create comment services
func NewCommentService(config config.Config) *CommentService {
	var repo repository.CommentRepository
	var query repository.CommentQuery

	db, err := db.InitCockroach(config.DATABASE_ADDR)
	if err != nil {
		logrus.Panic(err)
	}
	query = database.NewCommentQueryDatabase(db)
	repo = database.NewCommentRepositoryDatabase(db)

	return &CommentService{
		Query:      query,
		Repository: repo,
	}
}

// CreateComment to create comment
func (service *CommentService) CreateComment(userID, articleID, content string) (domain.Comment, error) {
	// Process
	result, err := domain.CreateComment(userID, articleID, content)
	if err != nil {
		return domain.Comment{}, err
	}
	// Persist
	err = service.Repository.Save(result)
	if err != nil {
		return domain.Comment{}, err
	}

	return *result, nil
}

//CountCommentsByArticleId to count all user
func (service *CommentService) CountCommentsByArticleId(ArticleId string) (int32, error) {
	id, err := uuid.FromString(ArticleId)
	if err != nil {
		return 0, err
	}

	result := service.Query.CountAllCommentsByArticleId(id)

	if result.Error != nil {
		log.Error(result)
		return 0, result.Error
	}

	return result.Result.(int32), nil
}

// FindCommentByIdArticle is to find comment by id
func (service *CommentService) FindCommentsByIdArticle(ArticleId string, Limit, Offset int32) ([]domain.Comment, error) {
	id, err := uuid.FromString(ArticleId)
	if err != nil {
		return []domain.Comment{}, err
	}

	result := service.Query.FindCommentsByIdArticle(id, Limit, Offset)
	if result.Error != nil {
		return []domain.Comment{}, result.Error
	}

	return result.Result.([]domain.Comment), nil
}
