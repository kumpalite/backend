package config

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

type Config struct {
	PORT          string
	DATABASE_ADDR string
}

func Load(mode string) Config {
	if mode == "dev" {
		err := godotenv.Load(".env")
		if err != nil {
			log.Println(err)
		}
	}

	PORT := os.Getenv("COMMENT_SERVICE_PORT")
	DATABASE_ADDR := os.Getenv("DATABASE_ADDR")

	if PORT == "" {
		PORT = ":9000"
	}

	return Config{
		PORT:          PORT,
		DATABASE_ADDR: DATABASE_ADDR,
	}
}
