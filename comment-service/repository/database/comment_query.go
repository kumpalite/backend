package database

import (
	"comment_service/domain"
	"comment_service/repository"
	"database/sql"
	"time"

	"github.com/satori/uuid"
)

type CommentQueryDatabase struct {
	database *sql.DB
}

func NewCommentQueryDatabase(db *sql.DB) repository.CommentQuery {
	return &CommentQueryDatabase{db}
}

func parseRow(row *sql.Row) (result repository.QueryResult) {
	var (
		total int32
	)
	err := row.Scan(&total)
	if err != nil {
		result.Error = err
		return
	}

	return repository.QueryResult{total, nil}
}

func parseRows(rows *sql.Rows) (result repository.QueryResult) {
	var comments []domain.Comment
	for rows.Next() {
		var (
			id        uuid.UUID
			content   string
			userID    uuid.UUID
			articleID uuid.UUID
			createdAt time.Time
		)
		err := rows.Scan(&id, &content, &articleID, &userID, &createdAt)
		if err != nil {
			result.Error = err
			return
		}

		newComment := domain.Comment{
			ID:        id,
			Content:   content,
			UserID:    userID,
			ArticleID: articleID,
			CreatedAt: createdAt,
		}

		comments = append(comments, newComment)
	}

	result.Result = comments

	return
}

func (query *CommentQueryDatabase) FindCommentsByIdArticle(id uuid.UUID, limit, offset int32) repository.QueryResult {
	var result repository.QueryResult
	rows, err := query.database.Query(
		`SELECT * FROM "Comment" WHERE article_id = $1 ORDER BY created_at desc limit $2 offset $3`,
		id,
		limit,
		offset,
	)

	if err != nil {
		result = repository.QueryResult{nil, err}
	}

	result = parseRows(rows)

	return result
}

func (query *CommentQueryDatabase) CountAllCommentsByArticleId(id uuid.UUID) repository.QueryResult {
	var result repository.QueryResult
	row := query.database.QueryRow(
		`SELECT count(*) from public."Comment" WHERE article_id = $1`,
		id,
	)

	result = parseRow(row)

	return result
}
