package database

import (
	"comment_service/domain"
	"comment_service/repository"
	"database/sql"
)

type CommentRepositoryDatabase struct {
	database *sql.DB
}

func NewCommentRepositoryDatabase(db *sql.DB) repository.CommentRepository {
	return &CommentRepositoryDatabase{db}
}

func (repository *CommentRepositoryDatabase) Save(comment *domain.Comment) error {
	_, execRun := repository.database.Exec(
		`INSERT INTO "Comment" VALUES($1, $2, $3, $4, $5)`,
		comment.ID,
		comment.Content,
		comment.ArticleID,
		comment.UserID,
		comment.CreatedAt,
	)
	err := execRun

	return err
}
