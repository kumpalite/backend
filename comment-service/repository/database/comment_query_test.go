package database

import (
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/satori/uuid"
	"github.com/stretchr/testify/assert"
)

func TestCanFindCommentByArticleID(t *testing.T) {

	columns := []string{"ID", "CONTENT", "ARTICLE_ID", "USER_ID"}

	id, _ := uuid.FromString("77081330-896f-4d9e-b1d3-d4da2eb5f896")
	db, mock, _ := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	mock.ExpectQuery(`SELECT * FROM "Comment" WHERE article_id = $1 limit $2 offset $3`).
		WithArgs(id, int32(1), int32(0)).
		WillReturnRows(
			sqlmock.NewRows(columns).AddRow(
				id,
				"Foobar",
				id,
				id,
			),
		)

	repo := NewCommentQueryDatabase(db)

	result := repo.FindCommentsByIdArticle(id, int32(1), int32(0))

	assert.Nil(t, result.Error)
}
