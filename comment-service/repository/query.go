package repository

import "github.com/satori/uuid"

// QueryResult is to wrap query result
type QueryResult struct {
	Result interface{}
	Error  error
}

// CommentQuery is contract for comment query
type CommentQuery interface {
	// single
	FindCommentsByIdArticle(id uuid.UUID, limit, offset int32) QueryResult
	CountAllCommentsByArticleId(id uuid.UUID) QueryResult
}
