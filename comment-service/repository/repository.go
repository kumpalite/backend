package repository

import "comment_service/domain"

// CommentRepository is wrap contract comment repository
type CommentRepository interface {
	Save(comment *domain.Comment) error
}
