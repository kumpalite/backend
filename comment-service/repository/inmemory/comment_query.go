package inmemory

import (
	"comment_service/domain"
	"comment_service/repository"
	"comment_service/storage"

	"github.com/satori/uuid"
)

// CommentQueryInMemory is comment query implementation in memory
type CommentQueryInMemory struct {
	Storage *storage.CommentStorage
}

// NewCommentQueryInMemory is to Create Instance CommentQueryInMemory
func NewCommentQueryInMemory(storage *storage.CommentStorage) repository.CommentQuery {
	return &CommentQueryInMemory{Storage: storage}
}

// FindCommentsByIdArticle is to find all comment
func (query *CommentQueryInMemory) FindCommentsByIdArticle(idArticle uuid.UUID) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)

	go func() {
		comment := []domain.Comment{}
		for _, item := range query.Storage.CommentMap {
			if idArticle == item.ArticleID {
				comment = append(comment, item)
			}
		}

		// if len(comment) == 0 {
		// 	result <- repository.QueryResult{Error: errors.New("Comment not found")}
		// } else {
		result <- repository.QueryResult{Result: comment}
		// }

		close(result)
	}()

	return result
}

// CountAllCommentsByArticleId is to count all user
func (query *CommentQueryInMemory) CountAllCommentsByArticleId(idArticle uuid.UUID) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)

	go func() {
		var total int32
		var comments []domain.Comment
		// total = int32(len(query.Storage.CommentMap))
		response := <-query.FindCommentsByIdArticle(idArticle)
		comments = response.Result.([]domain.Comment)
		total = int32(len(comments))
		result <- repository.QueryResult{Result: total}
		close(result)
	}()

	return result
}
