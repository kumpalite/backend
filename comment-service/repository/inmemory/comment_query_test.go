package inmemory

import (
	"comment_service/domain"
	"comment_service/repository"
	"comment_service/storage"
	"testing"

	"github.com/satori/uuid"
	"github.com/stretchr/testify/assert"
)

func TestCanFoundCommentByArticleId(t *testing.T) {
	// Given
	done := make(chan bool)

	comment1UID := uuid.NewV4()
	comment1UserID, _ := uuid.FromString("00000000-0000-0000-0000-000000000001")
	comment1ArticleID, _ := uuid.FromString("00000000-0000-0000-0000-000000000011")
	comment1 := domain.Comment{
		ID:        comment1UID,
		UserID:    comment1UserID,
		ArticleID: comment1ArticleID,
		Content:   "Ini content dari comment pertama",
	}

	comment2UID := uuid.NewV4()
	comment2UserID, _ := uuid.FromString("00000000-0000-0000-0000-000000000002")
	comment2ArticleID, _ := uuid.FromString("00000000-0000-0000-0000-000000000022")
	comment2 := domain.Comment{
		ID:        comment2UID,
		UserID:    comment2UserID,
		ArticleID: comment2ArticleID,
		Content:   "Ini content dari comment kedua",
	}

	storageComment := storage.CommentStorage{
		CommentMap: []domain.Comment{
			comment1, comment2,
		},
	}

	expect := storage.CommentStorage{
		CommentMap: []domain.Comment{
			comment1,
		},
	}

	query := NewCommentQueryInMemory(&storageComment)
	result := repository.QueryResult{}

	// When
	go func() {
		result = <-query.FindCommentsByIdArticle(comment1ArticleID)
		done <- true
	}()

	<-done

	// Then
	assert.Equal(t, expect.CommentMap, result.Result.([]domain.Comment))
}

func TestCanCountCommentsByArticleId(t *testing.T) {
	// Given
	done := make(chan bool)

	comment1UID := uuid.NewV4()
	comment1UserID, _ := uuid.FromString("00000000-0000-0000-0000-000000000001")
	comment1ArticleID, _ := uuid.FromString("00000000-0000-0000-0000-000000000011")
	comment1 := domain.Comment{
		ID:        comment1UID,
		UserID:    comment1UserID,
		ArticleID: comment1ArticleID,
		Content:   "Ini content dari comment pertama",
	}

	comment2UID := uuid.NewV4()
	comment2UserID, _ := uuid.FromString("00000000-0000-0000-0000-000000000002")
	// comment2ArticleID, _ := uuid.FromString("00000000-0000-0000-0000-000000000022")
	comment2 := domain.Comment{
		ID:        comment2UID,
		UserID:    comment2UserID,
		ArticleID: comment1ArticleID,
		Content:   "Ini content dari comment kedua",
	}

	storage := storage.CommentStorage{
		CommentMap: []domain.Comment{
			comment1, comment2,
		},
	}

	query := NewCommentQueryInMemory(&storage)
	result := repository.QueryResult{}

	// When
	go func() {
		result, _ = <-query.CountAllCommentsByArticleId(comment1ArticleID)
		done <- true
	}()

	<-done

	// Then
	assert.Equal(t, int32(2), result.Result)
}
