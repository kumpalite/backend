package inmemory

import (
	"comment_service/domain"
	"comment_service/storage"
	"testing"

	"github.com/satori/uuid"
	"github.com/stretchr/testify/assert"
)

func TestCanSaveCommentInRepository(t *testing.T) {
	// Given
	done := make(chan bool)
	commentUID := uuid.NewV4()
	commentUserID, _ := uuid.FromString("00000000-0000-0000-0000-000000000001")
	commentArticleID, _ := uuid.FromString("00000000-0000-0000-0000-000000000011")
	comment := domain.Comment{
		ID:        commentUID,
		UserID:    commentUserID,
		ArticleID: commentArticleID,
		Content:   "Ini content dari comment pertama",
	}

	db := storage.NewCommentStorage()
	repo := NewCommentRepositoryInMemory(db)

	// When
	var err error
	go func() {
		err = <-repo.Save(&comment)
		done <- true
	}()

	<-done

	// Then
	assert.Nil(t, err)
	assert.Equal(t, 4, len(db.CommentMap))
}
