package inmemory

import (
	"comment_service/domain"
	"comment_service/repository"
	"comment_service/storage"
)

// CommentRepositoryInMemory is comment repository implementation in memory
type CommentRepositoryInMemory struct {
	Storage *storage.CommentStorage
}

// NewCommentRepositoryInMemory is to create CommentRepositoryInMemory instance
func NewCommentRepositoryInMemory(storage *storage.CommentStorage) repository.CommentRepository {
	return &CommentRepositoryInMemory{Storage: storage}
}

// Save is for save comment
func (repo *CommentRepositoryInMemory) Save(comment *domain.Comment) <-chan error {
	result := make(chan error)

	go func() {
		repo.Storage.CommentMap = append(repo.Storage.CommentMap, *comment)

		result <- nil
		close(result)
	}()

	return result
}
