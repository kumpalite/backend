package domain

import (
	"time"

	"github.com/satori/uuid"
)

type Comment struct {
	ID        uuid.UUID
	Content   string
	UserID    uuid.UUID
	ArticleID uuid.UUID
	CreatedAt time.Time
}

func CreateComment(userID, articleID, content string) (*Comment, error) {
	uid := uuid.NewV4()

	userIDString, err := uuid.FromString(userID)
	if err != nil {
		return &Comment{}, err
	}

	articleIDString, err := uuid.FromString(articleID)
	if err != nil {
		return &Comment{}, err
	}

	return &Comment{
		ID:        uid,
		Content:   content,
		UserID:    userIDString,
		ArticleID: articleIDString,
		CreatedAt: time.Now(),
	}, nil

}
