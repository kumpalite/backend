package main

import "comment_service/server"
import "comment_service/config"
import "os"

func main() {
	args := os.Args[1:]

	var mode string
	if len(args) != 0 {
		mode = args[0]
	} else {
		mode = "prod"
	}

	server.Run(config.Load(mode))
}
