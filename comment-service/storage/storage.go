package storage

import (
	"comment_service/domain"
)

// CommentStorage is comment storage in memory
type CommentStorage struct {
	CommentMap []domain.Comment
}

// NewCommentStorage is to create comment storage
func NewCommentStorage() *CommentStorage {
	comment1, _ := domain.CreateComment("00000000-0000-0000-0000-000000000001", "00000000-0000-0000-0000-000000000011", "comment pertama")
	comment2, _ := domain.CreateComment("00000000-0000-0000-0000-000000000002", "00000000-0000-0000-0000-000000000022", "comment kedua")
	comment3, _ := domain.CreateComment("00000000-0000-0000-0000-000000000003", "00000000-0000-0000-0000-000000000033", "comment ketiga")
	comment4, _ := domain.CreateComment("00000000-0000-0000-0000-000000000001", "00000000-0000-0000-0000-000000000022", "comment keempat")
	comment5, _ := domain.CreateComment("00000000-0000-0000-0000-000000000001", "00000000-0000-0000-0000-000000000022", "comment kelima")

	return &CommentStorage{
		CommentMap: []domain.Comment{
			*comment1, *comment2, *comment3, *comment4, *comment5,
		},
	}
}
