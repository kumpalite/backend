compose:
	@cd ./graphql_service/ && make build
	@cd ./user_service/ && make build
	@cd ./article_service/ && make build
	@cd ./comment-service/ && make build
	@cd ./file_service/ && make build
	@docker-compose build
	@docker-compose up

