# Kumpalite Backend

## Architecture

![image](server.png)

## How to run
1. copy .env.example as .env
2. just run `make compose` in the root ofthis repo folder
and you can access it in http://localhost (running in :80)

## available route
### Auth
`POST /register`
use to regist a new user
example request payload:
```json
{
	"username": "username",
	"fullname": "username username",
	"email": "user@name.com",
	"password": "password"
}
```
example response payload:
```json
{
    "token": "some JWT token"
}
```

`POST /login`
use to regist a new user
example payload:
```json
{
	"identifier": "emailOrUsername",
	"password": "password"
}
```
example response payload:
```json
{
    "token": "some JWT token"
}
```

### File
`POST /file/upload`
use to upload new file
example request payload:
```
multipart
{
	"file": "your file",
}
```
example response payload:
```json
{
    "path": "your uploaded url"
}
```

`POST /login`
use to regist a new user
example payload:
```json
{
	"identifier": "emailOrUsername",
	"password": "password"
}
```

### Graphql
`GET /graphql`
use to access graphql playgrouhnd

`POST /graphql`
use to access graphql query  

some query may require an authorization. please add your token as following in the request header:
```json
{
    "Authorization": "your JWT token"
}
```
