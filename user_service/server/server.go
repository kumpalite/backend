package server

import (
	"context"
	"net"
	"user_service/config"
	pb "user_service/proto"
	"user_service/service"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	log "github.com/sirupsen/logrus"
	"golang.org/x/crypto/bcrypt"
)

type UserServiceServer struct {
	service *service.UserService
}

func NewServer(cfg config.Config) pb.UserServiceServer {
	service := service.NewUserService(cfg)
	return &UserServiceServer{
		service: service,
	}
}

func (server *UserServiceServer) CreateUser(ctx context.Context, param *pb.UserRequest) (*pb.UserResponse, error) {
	user, err := server.service.CreateUser(param.Fullname, param.Username, param.Email, param.Password)
	if err != nil {
		return nil, err
	}
	res := &pb.UserResponse{
		Id:       user.Id.String(),
		Fullname: user.Fullname,
		Username: user.Username,
		Email:    user.Email,
		Password: user.Password,
	}
	return res, nil
}
func (server *UserServiceServer) Authenticate(context context.Context, request *pb.AuthRequest) (*pb.UserResponse, error) {
	user, err := server.service.FindUserByIdentifier(request.Identifier)
	if err != nil {
		log.Error(user, err)
		return nil, err
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(request.Password))
	if err != nil {
		return nil, err
	}

	res := &pb.UserResponse{
		Id: user.Id.String(),
	}

	return res, nil
}

func (server *UserServiceServer) GetUserById(ctx context.Context, param *pb.UserRequest) (*pb.UserResponse, error) {
	user, err := server.service.FindUserByID(param.Id)
	if err != nil {
		return nil, err
	}

	res := &pb.UserResponse{
		Id:       user.Id.String(),
		Fullname: user.Fullname,
		Username: user.Username,
		Email:    user.Email,
		Password: user.Password,
	}
	return res, nil
}

func (server *UserServiceServer) GetUserByUsername(ctx context.Context, param *pb.UserRequest) (*pb.UserResponse, error) {
	user, err := server.service.FindUserByUsername(param.Username)
	if err != nil {
		return nil, err
	}

	res := &pb.UserResponse{
		Id:       user.Id.String(),
		Fullname: user.Fullname,
		Username: user.Username,
		Email:    user.Email,
		Password: user.Password,
	}
	return res, nil
}

func Run(config config.Config) {
	listen, err := net.Listen("tcp", config.PORT)
	if err != nil {
		log.Fatalf("ERROR %s", err)
	}

	server := grpc.NewServer()
	reflection.Register(server)
	userServer := NewServer(config)
	pb.RegisterUserServiceServer(server, userServer)

	log.Println("server is on ", config.PORT)

	log.Fatalf("err: %s", server.Serve(listen))
}
