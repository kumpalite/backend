package repository

import "github.com/satori/uuid"

// QueryResult is to wrap query result
type QueryResult struct {
	Result interface{}
	Error  error
}

type UserQuery interface {
	FindUserByID(uuid.UUID) <-chan QueryResult
	FindUserByIdentifier(identifier string) <-chan QueryResult
	FindUserByUsername(username string) <-chan QueryResult
}
