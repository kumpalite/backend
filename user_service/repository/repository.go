package repository

import "user_service/domain"

// UserRepository is wrap contract author repository
type UserRepository interface {
	Save(name *domain.User) <-chan error
	IsUsernameOrEmailExist(username, email string) bool
}
