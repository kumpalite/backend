package inmemory

import (
	"errors"
	"user_service/domain"
	"user_service/repository"
	"user_service/storage"
)

// UserRepositoryInMemory is user repository implementation in memory
type UserRepositoryInMemory struct {
	Storage *storage.UserStorage
}

// NewUserRepositoryInMemory is to create UserRepositoryInMemory instance
func NewUserRepositoryInMemory(storage *storage.UserStorage) repository.UserRepository {
	return &UserRepositoryInMemory{Storage: storage}
}

// Save is for save user
func (repo *UserRepositoryInMemory) Save(user *domain.User) <-chan error {
	result := make(chan error)

	if repo.IsUsernameOrEmailExist(user.Username, user.Email) == true {
		e := make(chan error)
		e <- errors.New("Username or Email is exist")
		return e
	}

	go func() {
		repo.Storage.UserMap = append(repo.Storage.UserMap, *user)

		result <- nil
		close(result)
	}()

	return result
}

//IsUsernameOrEmailExist is for check slug is exist or not
func (repo *UserRepositoryInMemory) IsUsernameOrEmailExist(username, email string) bool {
	for _, user := range repo.Storage.UserMap {
		if user.Username == username || user.Email == email {
			return true
		}
	}
	return false
}
