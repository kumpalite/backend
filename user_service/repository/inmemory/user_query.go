package inmemory

import (
	"errors"
	"user_service/domain"
	"user_service/repository"
	"user_service/storage"

	"github.com/satori/uuid"
)

// UserQueryInMemory is user query implementation in memory
type UserQueryInMemory struct {
	Storage *storage.UserStorage
}

// NewUserQueryInMemory is to Create Instance UserQueryInMemory
func NewUserQueryInMemory(storage *storage.UserStorage) repository.UserQuery {
	return &UserQueryInMemory{Storage: storage}
}

// FindUserByID is to find user by id
func (query *UserQueryInMemory) FindUserByID(id uuid.UUID) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)

	go func() {
		user := domain.User{}
		for _, item := range query.Storage.UserMap {
			if id == item.Id {
				user = item
			}
		}

		if user.Fullname == "" {
			result <- repository.QueryResult{Error: errors.New("User not found")}
		} else {
			result <- repository.QueryResult{Result: user}
		}

		close(result)
	}()

	return result
}

// FindUserByUsername is to find user by username
func (query *UserQueryInMemory) FindUserByUsername(username string) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)

	go func() {
		user := domain.User{}
		for _, item := range query.Storage.UserMap {
			if username == item.Username {
				user = item
			}
		}

		if user.Fullname == "" {
			result <- repository.QueryResult{Error: errors.New("User not found")}
		} else {
			result <- repository.QueryResult{Result: user}
		}

		close(result)
	}()

	return result
}

// FindUserByID is to find user by id
func (query *UserQueryInMemory) FindUserByIdentifier(identifier string) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)

	go func() {
		user := domain.User{}
		for _, item := range query.Storage.UserMap {
			if (item.Username == identifier) || (item.Email == identifier) {
				user = item
			}
		}

		if user.Fullname == "" {
			result <- repository.QueryResult{Error: errors.New("User not found")}
		} else {
			result <- repository.QueryResult{Result: user}
		}

		close(result)
	}()

	return result
}
