package inmemory

import (
	"testing"
	"user_service/domain"
	"user_service/storage"

	"github.com/satori/uuid"
	"github.com/stretchr/testify/assert"
)

func TestCanSaveUserInRepository(t *testing.T) {
	// Given
	done := make(chan bool)
	userUID := uuid.NewV4()
	user := domain.User{
		Id:       userUID,
		Fullname: "Foobar",
		Email:    "foobar@foobar.com",
		Username: "foo",
		Password: "pass",
	}

	db := storage.NewUserStorage()
	repo := NewUserRepositoryInMemory(db)

	// When
	var err error
	go func() {
		err = <-repo.Save(&user)
		done <- true
	}()

	<-done

	// Then
	assert.Nil(t, err)
	assert.Equal(t, 3, len(db.UserMap))
}

func TestCheckSlugIsExist(t *testing.T) {
	// Given
	done := make(chan bool)

	user1UID := uuid.NewV4()
	user1 := domain.User{
		Id:       user1UID,
		Fullname: "Foobar",
		Email:    "foobar@foobar.com",
		Username: "foo",
		Password: "pass",
	}
	user2UID := uuid.NewV4()
	user2 := domain.User{
		Id:       user2UID,
		Fullname: "Foobar2",
		Email:    "foobar2@foobar.com",
		Username: "foo",
		Password: "pass",
	}

	storage := storage.UserStorage{
		UserMap: []domain.User{
			user1, user2,
		},
	}

	repo := NewUserRepositoryInMemory(&storage)

	var result bool

	// When
	go func() {
		result = repo.IsUsernameOrEmailExist(user2.Username, user2.Email)
		done <- true
	}()

	<-done

	// Then
	assert.Equal(t, true, result)
}
