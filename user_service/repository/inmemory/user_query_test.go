package inmemory

import (
	"testing"
	"user_service/domain"
	"user_service/repository"
	"user_service/storage"

	"github.com/satori/uuid"
	"github.com/stretchr/testify/assert"
)

func TestCanFindUserByID(t *testing.T) {
	// Given
	done := make(chan bool)

	user1UID := uuid.NewV4()
	user1 := domain.User{
		Id:       user1UID,
		Fullname: "Foobar",
		Email:    "foobar@foobar.com",
		Username: "foo",
		Password: "pass",
	}

	user2UID := uuid.NewV4()
	user2 := domain.User{
		Id:       user2UID,
		Fullname: "Foobar 2",
		Email:    "foobar2@foobar.com",
		Username: "foo2",
		Password: "pass2",
	}

	storage := storage.UserStorage{
		UserMap: []domain.User{
			user1, user2,
		},
	}

	query := NewUserQueryInMemory(&storage)
	result := repository.QueryResult{}

	// When
	go func() {
		result, _ = <-query.FindUserByID(user2UID)
		done <- true
	}()

	<-done

	// Then
	assert.Equal(t, user2, result.Result.(domain.User))
}

func TestCanFindUserByEmail(t *testing.T) {
	// Given
	done := make(chan bool)

	user1UID := uuid.NewV4()
	user1 := domain.User{
		Id:       user1UID,
		Fullname: "Foobar",
		Email:    "foobar@foobar.com",
		Username: "foo",
		Password: "pass",
	}

	user2UID := uuid.NewV4()
	user2 := domain.User{
		Id:       user2UID,
		Fullname: "Foobar 2",
		Email:    "foobar2@foobar.com",
		Username: "foo2",
		Password: "pass2",
	}

	storage := storage.UserStorage{
		UserMap: []domain.User{
			user1, user2,
		},
	}

	query := NewUserQueryInMemory(&storage)
	result := repository.QueryResult{}

	// When
	go func() {
		result = <-query.FindUserByIdentifier(user2.Email)
		done <- true
	}()

	<-done

	// Then
	assert.NoError(t, result.Error)
	assert.Equal(t, user2, result.Result.(domain.User))
}

func TestCanFindUserByUsername(t *testing.T) {
	// Given
	done := make(chan bool)

	user1UID := uuid.NewV4()
	user1 := domain.User{
		Id:       user1UID,
		Fullname: "Foobar",
		Email:    "foobar@foobar.com",
		Username: "foo",
		Password: "pass",
	}

	user2UID := uuid.NewV4()
	user2 := domain.User{
		Id:       user2UID,
		Fullname: "Foobar 2",
		Email:    "foobar2@foobar.com",
		Username: "foo2",
		Password: "pass2",
	}

	storage := storage.UserStorage{
		UserMap: []domain.User{
			user1, user2,
		},
	}

	query := NewUserQueryInMemory(&storage)
	result := repository.QueryResult{}

	// When
	go func() {
		result = <-query.FindUserByIdentifier(user2.Username)
		done <- true
	}()

	<-done

	// Then
	assert.Equal(t, user2, result.Result.(domain.User))
}

func TestCanCountAllUser(t *testing.T) {
	// Given
	done := make(chan bool)

	user1UID := uuid.NewV4()
	user1 := domain.User{
		Id:       user1UID,
		Fullname: "Foobar",
		Email:    "foobar@foobar.com",
		Username: "foo",
		Password: "pass",
	}

	user2UID := uuid.NewV4()
	user2 := domain.User{
		Id:       user2UID,
		Fullname: "Foobar 2",
		Email:    "foobar2@foobar.com",
		Username: "foo2",
		Password: "pass2",
	}

	storage := storage.UserStorage{
		UserMap: []domain.User{
			user1, user2,
		},
	}

	query := NewUserQueryInMemory(&storage)
	result := repository.QueryResult{}

	// When
	go func() {
		result, _ = <-query.CountAllUser()
		done <- true
	}()

	<-done

	// Then
	assert.Equal(t, 2, result.Result)
}
