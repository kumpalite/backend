package database

import (
	"database/sql"
	"errors"
	"user_service/domain"
	"user_service/repository"
)

type UserRepositoryDatabase struct {
	database *sql.DB
}

func NewUserRepositoryDatabase(db *sql.DB) repository.UserRepository {
	return &UserRepositoryDatabase{db}
}

func (repository *UserRepositoryDatabase) Save(user *domain.User) <-chan error {
	err := make(chan error)
	go func() {
		var username, email string
		row := repository.database.QueryRow(`
			SELECT username, email FROM "User" WHERE username = $1 OR email = $2
		`, user.Username, user.Email)

		row.Scan(&username, &email)

		if username != user.Username && email != user.Email {
			_, execErr := repository.database.Exec(
				`INSERT INTO "User" VALUES($1, $2, $3, $4, $5)`,
				user.Id,
				user.Fullname,
				user.Username,
				user.Password,
				user.Email)
			err <- execErr
			close(err)
		} else {
			err <- errors.New("username or email already existed")
			close(err)
		}
	}()

	return err
}

func (repository *UserRepositoryDatabase) IsUsernameOrEmailExist(username, email string) bool {
	row := repository.database.QueryRow(`
			SELECT * FROM "User" WHERE username = $1 OR email = $2;
		`, username, email)

	if row == nil {
		return false
	}

	return true
}
