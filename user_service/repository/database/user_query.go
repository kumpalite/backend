package database

import (
	"database/sql"
	"user_service/domain"
	"user_service/repository"

	"github.com/satori/uuid"
)

type UserQueryDatabase struct {
	database *sql.DB
}

func NewUserQueryDatabase(database *sql.DB) repository.UserQuery {
	return &UserQueryDatabase{database}
}

func (query *UserQueryDatabase) FindUserByID(id uuid.UUID) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)
	uid := id.String()
	go func() {
		row := query.database.QueryRow(`
			SELECT * FROM "User" WHERE id = $1
		`, uid)

		result <- parseRow(row)

		close(result)
	}()

	return result
}

func (query *UserQueryDatabase) FindUserByUsername(username string) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)
	go func() {
		row := query.database.QueryRow(`
			SELECT * FROM "User" WHERE username = $1
		`, username)

		result <- parseRow(row)

		close(result)
	}()

	return result
}

func (query *UserQueryDatabase) FindUserByIdentifier(identifier string) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)
	go func() {
		row := query.database.QueryRow(`
			SELECT * FROM "User" WHERE email = $1 OR username = $1
		`, identifier)

		result <- parseRow(row)

		close(result)
	}()

	return result
}

func parseRow(row *sql.Row) (result repository.QueryResult) {
	var (
		id       uuid.UUID
		fullname string
		username string
		password string
		email    string
	)
	err := row.Scan(&id, &fullname, &username, &password, &email)
	if err != nil {
		result.Error = err
		return
	}

	newUser := domain.User{
		Id:       id,
		Fullname: fullname,
		Username: username,
		Password: password,
		Email:    email,
	}

	result.Result = newUser
	return
}
