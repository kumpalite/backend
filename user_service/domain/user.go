package domain

import (
	"github.com/satori/uuid"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	Id       uuid.UUID
	Fullname string
	Username string
	Email    string
	Password string
}

func CreateUser(fullname, username, email, password string) (*User, error) {

	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.MinCost)
	if err != nil {
		return nil, err
	}

	uid := uuid.NewV4()

	return &User{
		Id:       uid,
		Fullname: fullname,
		Username: username,
		Email:    email,
		Password: string(hash),
	}, nil

}
