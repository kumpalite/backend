package storage

import (
	"user_service/domain"

	"github.com/satori/uuid"
)

// UserStorage is to create user
type UserStorage struct {
	UserMap []domain.User
}

// NewUserStorage s
func NewUserStorage() *UserStorage {
	user1, _ := domain.CreateUser("Luthfi Nur Aida", "luthfi", "luthfi.aida@kumparan.com", "password")
	user2, _ := domain.CreateUser("Ganesha Danu", "danu", "ganesha_danu@kumparan.com", "password2")
	user3, _ := domain.CreateUser("Fifi", "fifi", "fifi@kumparan.com", "password3")
	user1.Id, _ = uuid.FromString("00000000-0000-0000-0000-000000000001")
	user2.Id, _ = uuid.FromString("00000000-0000-0000-0000-000000000002")
	user3.Id, _ = uuid.FromString("00000000-0000-0000-0000-000000000003")

	return &UserStorage{
		UserMap: []domain.User{
			*user1, *user2, *user3,
		},
	}
}
