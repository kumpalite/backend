package service

import (
	"user_service/config"
	db "user_service/database"
	"user_service/domain"
	"user_service/repository"
	"user_service/repository/database"

	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"

	"github.com/satori/uuid"
)

// UserService is
type UserService struct {
	Query      repository.UserQuery
	Repository repository.UserRepository
}

// NewUserService is to create user services
func NewUserService(cfg config.Config) *UserService {
	var repo repository.UserRepository
	var query repository.UserQuery

	db, err := db.InitCockroach(cfg.DATABASE_ADDR)
	if err != nil {
		logrus.Panic("fail to connect to server")
	}
	query = database.NewUserQueryDatabase(db)
	repo = database.NewUserRepositoryDatabase(db)

	return &UserService{
		Query:      query,
		Repository: repo,
	}
}

// FindUserByID is to find user by id
func (service *UserService) FindUserByID(uid string) (domain.User, error) {
	id, err := uuid.FromString(uid)
	logrus.Println(id)
	if err != nil {
		log.Error(err)
		return domain.User{}, err
	}

	result := <-service.Query.FindUserByID(id)

	logrus.Println(result)
	if result.Error != nil {
		log.Error(err)
		return domain.User{}, result.Error
	}

	return result.Result.(domain.User), nil
}

// FindUserByUsername is to find user by username
func (service *UserService) FindUserByUsername(username string) (domain.User, error) {
	result := <-service.Query.FindUserByUsername(username)

	logrus.Println(result)
	if result.Error != nil {
		log.Error(result.Error)
		return domain.User{}, result.Error
	}

	return result.Result.(domain.User), nil
}

// CreateUser to create user
func (service *UserService) CreateUser(fullname, username, email, password string) (domain.User, error) {
	// Process
	result, err := domain.CreateUser(fullname, username, email, password)
	if err != nil {
		log.Error(err)
		return domain.User{}, err
	}

	// Persist
	err = <-service.Repository.Save(result)
	if err != nil {
		log.Error(result)
		return domain.User{}, err
	}

	return *result, nil
}

//FindUserByIdentifier to find user by email or username
func (service *UserService) FindUserByIdentifier(identifier string) (domain.User, error) {
	result := <-service.Query.FindUserByIdentifier(identifier)

	if result.Error != nil {
		log.Error(result)
		return domain.User{}, result.Error
	}

	return result.Result.(domain.User), nil
}
