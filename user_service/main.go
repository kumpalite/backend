package main

import "user_service/server"
import "user_service/config"
import "os"

func main() {
	args := os.Args[1:]

	var mode string
	if len(args) != 0 {
		mode = args[0]
	} else {
		mode = "prod"
	}

	server.Run(config.Load(mode))
}
