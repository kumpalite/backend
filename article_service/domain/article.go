package domain

import (
	"fmt"
	"regexp"
	"strings"
	"time"

	"github.com/satori/uuid"
)

const (
	// PUBLISHED if the article is published
	PUBLISHED string = "PUBLISHED"
	// DRAFT if the article is draft
	DRAFT string = "DRAFT"
)

// Article is to wrap article
type Article struct {
	ID         uuid.UUID `json:"id"`
	Slug       string    `json:"slug"`
	Title      string    `json:"name"`
	Content    string    `json:"body"`
	Status     string    `json:"status"`
	UserID     uuid.UUID `json:"user_id"`
	CreatedAt  time.Time `json:"created_at"`
	UpdatedAt  time.Time `json:"updated_at"`
	Categories []string  `json:"categories"`
	Thumbnail  string    `json:thumbnail`
}

type UserCategory struct {
	UserID uuid.UUID `json:"user_id"`
	Name   string    `json:"name"`
	Slug   string    `json:"slug"`
}

func CreateCategory(userId uuid.UUID, name, slug string) *UserCategory {
	return &UserCategory{
		UserID: userId,
		Name:   name,
		Slug: slug,
	}
}

// CreateArticle is to create article instance
func CreateArticle(slug, title, content, status, userID string, categories []string, thumbnail string) (*Article, error) {
	uid := uuid.NewV4()
	if slug == "" {
		slug = GenerateSlug(SterelizedTitle(title))
	}

	if title == "" {
		return &Article{}, fmt.Errorf("Title can't be empty")
	}
	if len(title) < 10 || len(title) > 125 {
		return &Article{}, fmt.Errorf("Title must contain 10 - 125 characters")
	}

	// if content == "" {
	// 	return &Article{}, fmt.Errorf("Content can't be empty")
	// }
	// if len(content) < 200 || len(content) > 999 {
	// 	return &Article{}, fmt.Errorf("Content must contain 200 - 999 characters")
	// }

	userIDString, err := uuid.FromString(userID)
	if err != nil {
		return &Article{}, err
	}
	return &Article{
		ID:         uid,
		Title:      title,
		Slug:       slug,
		Content:    content,
		Status:     status,
		UserID:     userIDString,
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
		Categories: categories,
		Thumbnail:  thumbnail,
	}, nil

}

func SterelizedTitle(title string) string {
	r, _ := regexp.Compile(`(\w|\d|\s)*`)
	return r.FindString(title)
}

// GenerateSlug is generate slug article based on title
func GenerateSlug(title string) string {
	slug := strings.TrimSpace(title)
	slug = strings.ReplaceAll(slug, " ", "-")
	timeStamp := time.Now().UnixNano()
	timeString := fmt.Sprintf("%d", timeStamp)
	slug += "-" + timeString

	return strings.ToLower(slug)
}
