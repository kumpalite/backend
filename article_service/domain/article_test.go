package domain

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCanSaveArticleInRepository(t *testing.T) {
	// Given

	var categoriesUser1 []string
	categoriesUser1 = append(categoriesUser1, "")
	result, err := CreateArticle("pelita", "Pelita Harapan", "This is a free online calculator which counts the number of characters or letters in a text, useful for your tweets on Twitter, as well as a multitude of other applications. Whether it is Snapchat, Twitter, Facebook, ", DRAFT, "00000000-0000-0000-0000-000000000001", categoriesUser1)

	// When
	// Then
	assert.Nil(t, err)
	assert.Equal(t, "Pelita Harapan", result.Title)
}
