package server

import (
	"article_service/config"
	pb "article_service/proto"
	"article_service/service"
	"context"
	"fmt"
	"net"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	log "github.com/sirupsen/logrus"
)

type ArticleServiceServer struct {
	service *service.ArticleService
}

func NewServer(config *config.Config) pb.ArticleServiceServer {
	service := service.NewArticleService(config)
	return &ArticleServiceServer{
		service: service,
	}
}

func (server *ArticleServiceServer) CreateArticle(ctx context.Context, param *pb.ArticleRequest) (*pb.ArticleResponse, error) {
	var category []string
	for _, v := range param.Categories {
		category = append(category, v.Value)
	}

	searchedArticle, _ := server.service.FindArticleDetailBySlug(param.Slug)
	if searchedArticle.Slug == param.Slug && param.Slug != "" {
		param.Slug += fmt.Sprintf("%d", time.Now().UnixNano())
	}

	article, err := server.service.CreateArticle(param.Slug, param.Title, param.Content, param.Status, param.UserId, category, param.Thumbnail)
	if err != nil {
		return nil, err
	}

	var categoryResponse []*pb.Category
	for _, v := range article.Categories {
		c := pb.Category{Value: v}
		categoryResponse = append(categoryResponse, &c)
	}
	res := &pb.ArticleResponse{
		Id:         article.ID.String(),
		Title:      article.Title,
		Slug:       article.Slug,
		Content:    article.Content,
		CreatedAt:  article.CreatedAt.Format(time.RFC3339),
		UpdatedAt:  article.UpdatedAt.Format(time.RFC3339),
		Status:     article.Status,
		UserId:     article.UserID.String(),
		Categories: categoryResponse,
		Thumbnail:  article.Thumbnail,
	}
	return res, nil
}

func (server *ArticleServiceServer) UpdateArticle(ctx context.Context, param *pb.ArticleRequest) (*pb.ArticleResponse, error) {
	var category []string
	for _, v := range param.Categories {
		category = append(category, v.Value)
	}

	searchedArticle, _ := server.service.FindArticleDetailBySlug(param.Slug)
	if searchedArticle.Slug == param.Slug && param.Slug != "" {
		param.Slug += fmt.Sprintf("%d", time.Now().UnixNano())
	}

	article, err := server.service.UpdateArticle(param.Id, param.Slug, param.Title, param.Content, param.Status, category, param.Thumbnail)
	if err != nil {
		return nil, err
	}

	var categoryResponse []*pb.Category
	for _, v := range article.Categories {
		c := pb.Category{Value: v}
		categoryResponse = append(categoryResponse, &c)
	}
	res := &pb.ArticleResponse{
		Id:         article.ID.String(),
		Title:      article.Title,
		Slug:       article.Slug,
		Content:    article.Content,
		CreatedAt:  article.CreatedAt.Format(time.RFC3339),
		UpdatedAt:  article.UpdatedAt.Format(time.RFC3339),
		Status:     article.Status,
		UserId:     article.UserID.String(),
		Categories: categoryResponse,
		Thumbnail:  article.Thumbnail,
	}
	return res, nil
}

func (server *ArticleServiceServer) CreateCategory(ctx context.Context, param *pb.Category) (*pb.Category, error) {
	category, err := server.service.CreateCategory(param.UserId, param.Value, param.Slug)
	if err != nil {
		return nil, err
	}

	res := &pb.Category{
		UserId: category.UserID.String(),
		Value:  category.Name,
		Slug:   category.Slug,
	}
	return res, nil
}

func (server *ArticleServiceServer) GetArticleDetailById(ctx context.Context, param *pb.ArticleRequest) (*pb.ArticleResponse, error) {
	article, err := server.service.FindArticleDetailByID(param.Id)
	if err != nil {
		return nil, err
	}

	var categoryResponse []*pb.Category
	for _, v := range article.Categories {
		c := pb.Category{Value: v}
		categoryResponse = append(categoryResponse, &c)
	}
	res := &pb.ArticleResponse{
		Id:         article.ID.String(),
		Title:      article.Title,
		Slug:       article.Slug,
		Content:    article.Content,
		CreatedAt:  article.CreatedAt.Format(time.RFC3339),
		UpdatedAt:  article.UpdatedAt.Format(time.RFC3339),
		Status:     article.Status,
		UserId:     article.UserID.String(),
		Categories: categoryResponse,
		Thumbnail:  article.Thumbnail,
	}
	return res, nil
}

func (server *ArticleServiceServer) GetArticleDetailBySlug(ctx context.Context, param *pb.ArticleRequest) (*pb.ArticleResponse, error) {
	article, err := server.service.FindArticleDetailBySlug(param.Slug)
	if err != nil {
		return nil, err
	}

	var categoryResponse []*pb.Category
	for _, v := range article.Categories {
		c := pb.Category{Value: v}
		categoryResponse = append(categoryResponse, &c)
	}
	res := &pb.ArticleResponse{
		Id:         article.ID.String(),
		Title:      article.Title,
		Slug:       article.Slug,
		Content:    article.Content,
		CreatedAt:  article.CreatedAt.Format(time.RFC3339),
		UpdatedAt:  article.UpdatedAt.Format(time.RFC3339),
		Status:     article.Status,
		UserId:     article.UserID.String(),
		Categories: categoryResponse,
		Thumbnail:  article.Thumbnail,
	}
	return res, nil
}

func (server *ArticleServiceServer) GetArticleByStatus(ctx context.Context, param *pb.ArticleRequest) (*pb.ArticleListResponse, error) {
	articlesFromService, err := server.service.GetArticlesByStatus(param.Status, int(param.Offset), int(param.Limit))
	if err != nil {
		return nil, err
	}

	var articles []*pb.ArticleResponse
	for _, v := range articlesFromService {
		var categoryResponse []*pb.Category
		for _, c := range v.Categories {
			category := pb.Category{Value: c}
			categoryResponse = append(categoryResponse, &category)
		}

		a := &pb.ArticleResponse{
			Id:         v.ID.String(),
			Title:      v.Title,
			Slug:       v.Slug,
			Content:    v.Content,
			CreatedAt:  v.CreatedAt.Format(time.RFC3339),
			UpdatedAt:  v.UpdatedAt.Format(time.RFC3339),
			Status:     v.Status,
			UserId:     v.UserID.String(),
			Categories: categoryResponse,
			Thumbnail:  v.Thumbnail,
		}
		articles = append(articles, a)
	}

	res := &pb.ArticleListResponse{
		ArticleList: articles,
	}
	return res, nil
}

func (server *ArticleServiceServer) GetArticleByCategory(ctx context.Context, param *pb.Category) (*pb.ArticleListResponse, error) {
	articlesFromService, err := server.service.GetArticlesByCategory(param.Value, int(param.Offset), int(param.Limit))
	if err != nil {
		return nil, err
	}

	var articles []*pb.ArticleResponse
	for _, v := range articlesFromService {
		var categoryResponse []*pb.Category
		for _, c := range v.Categories {
			category := pb.Category{Value: c}
			categoryResponse = append(categoryResponse, &category)
		}

		a := &pb.ArticleResponse{
			Id:         v.ID.String(),
			Title:      v.Title,
			Slug:       v.Slug,
			Content:    v.Content,
			CreatedAt:  v.CreatedAt.Format(time.RFC3339),
			UpdatedAt:  v.UpdatedAt.Format(time.RFC3339),
			Status:     v.Status,
			UserId:     v.UserID.String(),
			Categories: categoryResponse,
			Thumbnail:  v.Thumbnail,
		}
		articles = append(articles, a)
	}

	res := &pb.ArticleListResponse{
		ArticleList: articles,
	}
	return res, nil
}

func (server *ArticleServiceServer) GetArticleByUserId(ctx context.Context, param *pb.ArticleRequest) (*pb.ArticleListResponse, error) {
	articlesFromService, err := server.service.GetArticlesByUserId(param.UserId, int(param.Offset), int(param.Limit))
	if err != nil {
		return nil, err
	}

	var articles []*pb.ArticleResponse
	for _, v := range articlesFromService {
		var categoryResponse []*pb.Category
		for _, c := range v.Categories {
			category := pb.Category{Value: c}
			categoryResponse = append(categoryResponse, &category)
		}

		a := &pb.ArticleResponse{
			Id:         v.ID.String(),
			Title:      v.Title,
			Slug:       v.Slug,
			Content:    v.Content,
			CreatedAt:  v.CreatedAt.Format(time.RFC3339),
			UpdatedAt:  v.UpdatedAt.Format(time.RFC3339),
			Status:     v.Status,
			UserId:     v.UserID.String(),
			Categories: categoryResponse,
			Thumbnail:  v.Thumbnail,
		}
		articles = append(articles, a)
	}

	res := &pb.ArticleListResponse{
		ArticleList: articles,
	}
	return res, nil
}

func (server *ArticleServiceServer) GetCategoryByUserId(ctx context.Context, param *pb.Category) (*pb.CategoryListResponse, error) {
	categoriesFromService, err := server.service.GetCategoriesByUserId(param.UserId, int(param.Offset), int(param.Limit))
	if err != nil {
		return nil, err
	}

	var categories []*pb.Category
	for _, v := range categoriesFromService {
		c := &pb.Category{
			UserId: v.UserID.String(),
			Value:  v.Name,
			Slug:   v.Slug,
		}
		categories = append(categories, c)
	}

	res := &pb.CategoryListResponse{
		CategoryList: categories,
	}
	return res, nil
}

func (server *ArticleServiceServer) GetCategoryBySlug(ctx context.Context, param *pb.Category) (*pb.CategoryListResponse, error) {
	categoriesFromService, err := server.service.GetCategoriesBySlug(param.Slug)
	if err != nil {
		return nil, err
	}

	var categories []*pb.Category
	for _, v := range categoriesFromService {
		c := &pb.Category{
			UserId: v.UserID.String(),
			Value:  v.Name,
			Slug:   v.Slug,
		}
		categories = append(categories, c)
	}

	res := &pb.CategoryListResponse{
		CategoryList: categories,
	}
	return res, nil
}

func (server *ArticleServiceServer) GetAll(ctx context.Context, param *pb.ArticleRequest) (*pb.ArticleListResponse, error) {
	articlesFromService, err := server.service.GetAllArticles(int(param.Offset), int(param.Limit))

	if err != nil {
		return nil, err
	}

	var articles []*pb.ArticleResponse
	for _, v := range articlesFromService {
		var categoryResponse []*pb.Category
		for _, c := range v.Categories {
			category := pb.Category{Value: c}
			categoryResponse = append(categoryResponse, &category)
		}

		a := &pb.ArticleResponse{
			Id:         v.ID.String(),
			Title:      v.Title,
			Slug:       v.Slug,
			Content:    v.Content,
			CreatedAt:  v.CreatedAt.Format(time.RFC3339),
			UpdatedAt:  v.UpdatedAt.Format(time.RFC3339),
			Status:     v.Status,
			UserId:     v.UserID.String(),
			Categories: categoryResponse,
			Thumbnail:  v.Thumbnail,
		}
		articles = append(articles, a)
	}

	res := &pb.ArticleListResponse{
		ArticleList: articles,
	}
	return res, nil
}

func (server *ArticleServiceServer) Search(ctx context.Context, param *pb.Keyword) (*pb.ArticleListResponse, error) {
	articlesFromService, err := server.service.Search(param.Keyword, int(param.Offset), int(param.Limit))
	if err != nil {
		return nil, err
	}

	var articles []*pb.ArticleResponse
	for _, v := range articlesFromService {
		var categoryResponse []*pb.Category
		for _, c := range v.Categories {
			category := pb.Category{Value: c}
			categoryResponse = append(categoryResponse, &category)
		}

		a := &pb.ArticleResponse{
			Id:         v.ID.String(),
			Title:      v.Title,
			Slug:       v.Slug,
			Content:    v.Content,
			CreatedAt:  v.CreatedAt.Format(time.RFC3339),
			UpdatedAt:  v.UpdatedAt.Format(time.RFC3339),
			Status:     v.Status,
			UserId:     v.UserID.String(),
			Categories: categoryResponse,
			Thumbnail:  v.Thumbnail,
		}
		articles = append(articles, a)
	}

	res := &pb.ArticleListResponse{
		ArticleList: articles,
	}
	return res, nil
}

func Run(config config.Config) {
	listen, err := net.Listen("tcp", config.PORT)
	if err != nil {
		log.Fatalf("ERROR %s", err)
	}

	server := grpc.NewServer()
	reflection.Register(server)
	articleServer := NewServer(&config)
	pb.RegisterArticleServiceServer(server, articleServer)

	log.Println("server is on ", config.PORT)

	log.Fatalf("err: %s", server.Serve(listen))
}
