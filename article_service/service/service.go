package service

import (
	"article_service/config"
	db "article_service/database"
	"article_service/domain"
	"article_service/repository"
	"article_service/repository/database"
	cache "article_service/repository/redis"
	"time"

	"github.com/gomodule/redigo/redis"
	"github.com/satori/uuid"
	"github.com/sirupsen/logrus"
)

// ArticleService is
type ArticleService struct {
	Query      repository.ArticleQuery
	Cache      repository.ArticleCache
	Repository repository.ArticleRepository
}

func newPool(ADDR string) *redis.Pool {
	return &redis.Pool{
		// Maximum number of idle connections in the pool.
		MaxIdle: 80,
		// max number of connections
		MaxActive: 12000,
		// Dial is an application supplied function for creating and
		// configuring a connection.
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", ADDR)
			if err != nil {
				panic(err.Error())
			}
			return c, err
		},
	}
}

// NewArticleService is to create article services
func NewArticleService(config *config.Config) *ArticleService {
	var repo repository.ArticleRepository
	var query repository.ArticleQuery

	db, err := db.InitCockroach(config.DATABASE_ADDR)
	if err != nil {
		logrus.Panic(err)
	}

	redis := newPool(config.REDIS_ADDR)
	cache := cache.New(redis)

	query = database.NewArticleQueryDatabase(db)
	repo = database.NewArticleRepositoryDatabase(db)

	return &ArticleService{
		Query:      query,
		Repository: repo,
		Cache:      cache,
	}
}

// GetAllArticles is to find all articles
func (service *ArticleService) GetAllArticles(offset, limit int) ([]domain.Article, error) {
	// Process
	result := service.Query.FindAllArticles(offset, limit)
	logrus.Println(result)
	// if result.Error != nil {
	// 	return []domain.Article{}, result.Error
	// }

	// Return
	return result.Result.([]domain.Article), nil
}

// FindArticleDetailByID is to find article by id
func (service *ArticleService) FindArticleDetailByID(uid string) (domain.Article, error) {
	id, err := uuid.FromString(uid)
	if err != nil {
		return domain.Article{}, err
	}

	result := service.Query.FindArticleDetailByID(id)
	if result.Error != nil {
		return domain.Article{}, result.Error
	}

	return result.Result.(domain.Article), nil
}

// FindArticleDetailBySlug is to find article by id
func (service *ArticleService) FindArticleDetailBySlug(slug string) (domain.Article, error) {

	result, err := service.Cache.Get(slug)
	if err != nil {
		return result, err

	} else {
		result := service.Query.FindArticleDetailBySlug(slug)
		if result.Error != nil {
			return domain.Article{}, result.Error
		}

		_ = service.Cache.Save(result.Result.(domain.Article))

		return result.Result.(domain.Article), nil
	}
}

// CreateArticle to create article
func (service *ArticleService) CreateArticle(slug, title, content, status, userID string, categories []string, thumbnail string) (domain.Article, error) {
	// Process
	result, err := domain.CreateArticle(slug, title, content, status, userID, categories, thumbnail)
	if err != nil {
		return domain.Article{}, err
	}
	// Persist
	err = service.Repository.Save(result)
	if err != nil {
		return domain.Article{}, err
	}

	return *result, nil
}

// UpdateArticle to update article
func (service *ArticleService) UpdateArticle(id, slug, title, content, status string, categories []string, thumbnail string) (domain.Article, error) {
	idArticle, err := uuid.FromString(id)
	if err != nil {
		return domain.Article{}, err
	}

	// Persist
	article := domain.Article{
		ID:         idArticle,
		Slug:       slug,
		Title:      title,
		Content:    content,
		Status:     status,
		Categories: categories,
		Thumbnail:  thumbnail,
		UpdatedAt:  time.Now(),
	}
	err = service.Repository.Update(&article)
	if err != nil {
		return domain.Article{}, err
	}

	return article, nil
}

// CreateCategory to create category
func (service *ArticleService) CreateCategory(userID, name, slug string) (domain.UserCategory, error) {
	idUser, err := uuid.FromString(userID)
	if err != nil {
		return domain.UserCategory{}, err
	}
	// Process
	result := domain.CreateCategory(idUser, name, slug)
	// Persist
	err = service.Repository.SaveCategory(result)
	if err != nil {
		return domain.UserCategory{}, err
	}

	return *result, nil
}

//GetArticlesByStatus to get articles by status
func (service *ArticleService) GetArticlesByStatus(status string, offset, limit int) ([]domain.Article, error) {
	// Process
	result := service.Query.FindArticlesByStatus(status, offset, limit)
	if result.Error != nil {
		return []domain.Article{}, result.Error
	}

	// Return
	return result.Result.([]domain.Article), nil
}

//GetArticlesByCategory to get articles by category
func (service *ArticleService) GetArticlesByCategory(category string, offset, limit int) ([]domain.Article, error) {
	// Process
	result := service.Query.FindArticlesByCategory(category, offset, limit)
	if result.Error != nil {
		return []domain.Article{}, result.Error
	}

	// Return
	return result.Result.([]domain.Article), nil
}

//GetArticlesByUserId to get articles by user id
func (service *ArticleService) GetArticlesByUserId(userId string, offset, limit int) ([]domain.Article, error) {
	idUser, err := uuid.FromString(userId)
	if err != nil {
		return []domain.Article{}, err
	}
	// Process
	result := service.Query.FindArticlesByUserId(idUser, offset, limit)
	logrus.Println(result)
	if result.Error != nil {
		return []domain.Article{}, result.Error
	}

	// Return
	return result.Result.([]domain.Article), nil
}

//GetCategoriesByUserId to get categories by user id
func (service *ArticleService) GetCategoriesByUserId(userId string, offset, limit int) ([]domain.UserCategory, error) {
	idUser, err := uuid.FromString(userId)
	if err != nil {
		return []domain.UserCategory{}, err
	}
	// Process
	result := service.Query.FindCategoriesByUserId(idUser, offset, limit)
	if result.Error != nil {
		return []domain.UserCategory{}, result.Error
	}

	// Return
	return result.Result.([]domain.UserCategory), nil
}

//GetCategoriesBySlug to get categories by slug
func (service *ArticleService) GetCategoriesBySlug(slug string) ([]domain.UserCategory, error) {

	// Process
	result := service.Query.FindCategoriesBySlug(slug)
	if result.Error != nil {
		return []domain.UserCategory{}, result.Error
	}

	// Return
	return result.Result.([]domain.UserCategory), nil
}

//Search to get articles by user id
func (service *ArticleService) Search(keyword string, offset, limit int) ([]domain.Article, error) {
	// Process
	result := service.Query.Search(keyword, offset, limit)
	logrus.Println(result)
	if result.Error != nil {
		return []domain.Article{}, result.Error
	}

	// Return
	return result.Result.([]domain.Article), nil
}
