package inmemory

import (
	"article_service/domain"
	"article_service/repository"
	"article_service/storage"
	"errors"
)

// ArticleRepositoryInMemory is article repository implementation in memory
type ArticleRepositoryInMemory struct {
	Storage *storage.ArticleStorage
}

// NewArticleRepositoryInMemory is to create ArticleRepositoryInMemory instance
func NewArticleRepositoryInMemory(storage *storage.ArticleStorage) repository.ArticleRepository {
	return &ArticleRepositoryInMemory{Storage: storage}
}

// Save is for save article
func (repo *ArticleRepositoryInMemory) Save(article *domain.Article) <-chan error {
	result := make(chan error)

	if repo.IsSlugExist(article.Slug) == true {
		e := make(chan error)
		e <- errors.New("Slug is exist")
		return e
	}

	go func() {
		repo.Storage.ArticleMap = append(repo.Storage.ArticleMap, *article)

		result <- nil
		close(result)
	}()

	return result
}

// Update is for update article
func (repo *ArticleRepositoryInMemory) Update(article *domain.Article) <-chan error {
	// query := NewArticleQueryInMemory(repo.Storage)
	result := make(chan error)

	// var finalResult []domain.Article
	// go func() {
	// 	// repo.Storage.ArticleMap = append(repo.Storage.ArticleMap, *article)
	// 	resultArticle := query.FindArticleDetailByID(article.ID)

	// 	for i, v := range repo.Storage.ArticleMap {
	// 		if v.ID == article.ID {
	// 			finalResult = append(finalResult, resultArticle.Result.(domain.Article))
	// 		} else {
	// 			finalResult = append(finalResult, v)
	// 		}
	// 	}

	// 	result <- nil
	// 	close(result)
	// }()

	return result
}

//IsSlugExist is for check slug is exist or not
func (repo *ArticleRepositoryInMemory) IsSlugExist(slug string) bool {
	for _, article := range repo.Storage.ArticleMap {
		if article.Slug == slug {
			return true
		}
	}
	return false
}

// SaveCategory is for save article
func (repo *ArticleRepositoryInMemory) SaveCategory(category *domain.UserCategory) <-chan error {
	result := make(chan error)

	go func() {
		repo.Storage.CategoryMap = append(repo.Storage.CategoryMap, *category)

		result <- nil
		close(result)
	}()

	return result
}

//IsCategoryExist is for check category is exist or not
func (repo *ArticleRepositoryInMemory) IsCategoryExist(category, slug string) bool {
	for _, categories := range repo.Storage.CategoryMap {
		if categories.Name == category && categories.Slug == slug {
			return true
		}
	}
	return false
}
