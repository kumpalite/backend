package inmemory

import (
	"article_service/domain"
	"article_service/repository"
	"article_service/storage"
	"errors"

	"github.com/satori/uuid"
)

// ArticleQueryInMemory is article query implementation in memory
type ArticleQueryInMemory struct {
	Storage *storage.ArticleStorage
}

// NewArticleQueryInMemory is to Create Instance ArticleQueryInMemory
func NewArticleQueryInMemory(storage *storage.ArticleStorage) repository.ArticleQuery {
	return &ArticleQueryInMemory{Storage: storage}
}

// FindAllArticles is to find all article
func (query *ArticleQueryInMemory) FindAllArticles() <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)

	go func() {
		result <- repository.QueryResult{
			Result: query.Storage.ArticleMap,
		}
		close(result)
	}()
	return result
}

// FindArticlesByStatus is to find article by status
func (query *ArticleQueryInMemory) FindArticlesByStatus(status string) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)

	var articles []domain.Article

	for _, v := range query.Storage.ArticleMap {
		if v.Status == status {
			articles = append(articles, v)
		}
	}
	go func() {
		result <- repository.QueryResult{
			Result: articles,
		}
		close(result)
	}()

	return result
}

// FindArticleDetailByID is to find detail article by id
func (query *ArticleQueryInMemory) FindArticleDetailByID(id uuid.UUID) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)

	go func() {
		article := domain.Article{}
		for _, item := range query.Storage.ArticleMap {
			if id == item.ID {
				article = item
			}
		}

		if article.Title == "" {
			result <- repository.QueryResult{Error: errors.New("Article not found")}
		} else {
			result <- repository.QueryResult{Result: article}
		}

		close(result)
	}()

	return result
}

// FindArticleDetailBySlug is to find detail article by id
func (query *ArticleQueryInMemory) FindArticleDetailBySlug(slug string) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)

	go func() {
		article := domain.Article{}
		for _, item := range query.Storage.ArticleMap {
			if slug == item.Slug {
				article = item
			}
		}

		if article.Title == "" {
			result <- repository.QueryResult{Error: errors.New("Article not found")}
		} else {
			result <- repository.QueryResult{Result: article}
		}

		close(result)
	}()

	return result
}

// FindArticlesByCategory is to find article by category
func (query *ArticleQueryInMemory) FindArticlesByCategory(category string) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)

	var articles []domain.Article

	for _, v := range query.Storage.ArticleMap {
		for _, c := range v.Categories {
			if c == category {
				articles = append(articles, v)
			}
		}
	}
	go func() {
		result <- repository.QueryResult{
			Result: articles,
		}
		close(result)
	}()

	return result
}

// FindArticlesByUserId is to find article by user id
func (query *ArticleQueryInMemory) FindArticlesByUserId(idUser uuid.UUID) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)

	var articles []domain.Article

	for _, v := range query.Storage.ArticleMap {
		if v.UserID == idUser {
			articles = append(articles, v)
		}
	}
	go func() {
		result <- repository.QueryResult{
			Result: articles,
		}
		close(result)
	}()

	return result
}

// FindCategoriesByUserId is to find article by user id
func (query *ArticleQueryInMemory) FindCategoriesByUserId(idUser uuid.UUID) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)

	var categories []domain.UserCategory

	for _, v := range query.Storage.CategoryMap {
		if v.UserID == idUser {
			categories = append(categories, v)
		}
	}
	go func() {
		result <- repository.QueryResult{
			Result: categories,
		}
		close(result)
	}()

	return result
}

// FindCategoriesBySlug is to find category by slug
func (query *ArticleQueryInMemory) FindCategoriesBySlug(slug string) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)

	var categories []domain.UserCategory

	for _, v := range query.Storage.CategoryMap {
		if v.Slug == slug {
			categories = append(categories, v)
		}
	}
	go func() {
		result <- repository.QueryResult{
			Result: categories,
		}
		close(result)
	}()

	return result
}

// Search is to find article by category
func (query *ArticleQueryInMemory) Search(keyword string) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)

	// var articles []domain.Article

	// for _, v := range query.Storage.ArticleMap {
	// 	for _, c := range v.Categories {
	// 		if c == category {
	// 			articles = append(articles, v)
	// 		}
	// 	}
	// }
	// go func() {
	// 	result <- repository.QueryResult{
	// 		Result: articles,
	// 	}
	// 	close(result)
	// }()

	return result
}
