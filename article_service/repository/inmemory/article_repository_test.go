package inmemory

import (
	"article_service/domain"
	"article_service/storage"
	"testing"
	"time"

	"github.com/satori/uuid"
	"github.com/stretchr/testify/assert"
)

func TestCanSaveArticleInRepository(t *testing.T) {
	// Given
	done := make(chan bool)
	articleUID := uuid.NewV4()
	articleUserID, _ := uuid.FromString("00000000-0000-0000-0000-000000000001")
	article := domain.Article{
		ID:         articleUID,
		Title:      "Foobar",
		Slug:       "foobar",
		Content:    "body article",
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
		Status:     domain.PUBLISHED,
		UserID:     articleUserID,
		Categories: []string{"News", "Politic"},
	}

	db := storage.NewArticleStorage()
	repo := NewArticleRepositoryInMemory(db)

	// When
	var err error
	go func() {
		err = <-repo.Save(&article)
		done <- true
	}()

	<-done

	// Then
	assert.Nil(t, err)
	assert.Equal(t, 4, len(db.ArticleMap))
}

func TestCheckSlugIsExist(t *testing.T) {
	// Given
	done := make(chan bool)

	article1UID := uuid.NewV4()
	article1UserID, _ := uuid.FromString("00000000-0000-0000-0000-000000000001")
	article1 := domain.Article{
		ID:         article1UID,
		Title:      "Foobar",
		Slug:       "foobar",
		Content:    "body article",
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
		Status:     domain.PUBLISHED,
		UserID:     article1UserID,
		Categories: []string{"News", "Politic"},
	}

	article2UID := uuid.NewV4()
	article2UserID, _ := uuid.FromString("00000000-0000-0000-0000-000000000002")
	article2 := domain.Article{
		ID:         article2UID,
		Title:      "Foobar 2",
		Slug:       "foobar",
		Content:    "body article 2",
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
		Status:     domain.PUBLISHED,
		UserID:     article2UserID,
		Categories: []string{},
	}

	storage := storage.ArticleStorage{
		ArticleMap: []domain.Article{
			article1,
		},
	}

	repo := NewArticleRepositoryInMemory(&storage)

	var result bool

	// When
	go func() {
		result = repo.IsSlugExist(article2.Slug)
		done <- true
	}()

	<-done

	// Then
	assert.Equal(t, true, result)
}

func TestCanSaveCategoryInRepository(t *testing.T) {
	// Given
	done := make(chan bool)
	UserID, _ := uuid.FromString("00000000-0000-0000-0000-000000000001")
	category := domain.UserCategory{
		UserID:     UserID,
		Name: "KESEHATAN",
	}

	db := storage.NewArticleStorage()
	repo := NewArticleRepositoryInMemory(db)

	// When
	var err error
	go func() {
		err = <-repo.SaveCategory(&category)
		done <- true
	}()

	<-done

	// Then
	assert.Nil(t, err)
	assert.Equal(t, 1, len(db.CategoryMap))
}
