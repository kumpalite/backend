package inmemory

import (
	"article_service/domain"
	"article_service/repository"
	"article_service/storage"
	"testing"
	"time"

	"github.com/satori/uuid"
	"github.com/stretchr/testify/assert"
)

func TestCanFoundAllArticle(t *testing.T) {
	// Given
	done := make(chan bool)

	article1UID := uuid.NewV4()
	article1UserID, _ := uuid.FromString("00000000-0000-0000-0000-000000000001")
	article1 := domain.Article{
		ID:         article1UID,
		Title:      "Foobar",
		Slug:       "foobar",
		Content:    "body article",
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
		Status:     domain.PUBLISHED,
		UserID:     article1UserID,
		Categories: []string{"News", "Politic"},
	}

	article2UID := uuid.NewV4()
	article2UserID, _ := uuid.FromString("00000000-0000-0000-0000-000000000002")
	article2 := domain.Article{
		ID:         article2UID,
		Title:      "Foobar 2",
		Slug:       "foobar-2",
		Content:    "body article 2",
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
		Status:     domain.PUBLISHED,
		UserID:     article2UserID,
		Categories: []string{},
	}

	storage := storage.ArticleStorage{
		ArticleMap: []domain.Article{
			article1, article2,
		},
	}

	query := NewArticleQueryInMemory(&storage)
	result := repository.QueryResult{}

	// When
	go func() {
		result = <-query.FindAllArticles()
		done <- true
	}()

	<-done

	// Then
	assert.Equal(t, storage.ArticleMap, result.Result.([]domain.Article))
}

func TestCanFoundDetailArticleByID(t *testing.T) {
	// Given
	done := make(chan bool)

	article1UID := uuid.NewV4()
	article1UserID, _ := uuid.FromString("00000000-0000-0000-0000-000000000001")
	article1 := domain.Article{
		ID:         article1UID,
		Title:      "Foobar",
		Slug:       "foobar",
		Content:    "body article",
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
		Status:     domain.PUBLISHED,
		UserID:     article1UserID,
		Categories: []string{"News", "Politic"},
	}

	article2UID := uuid.NewV4()
	article2UserID, _ := uuid.FromString("00000000-0000-0000-0000-000000000002")
	article2 := domain.Article{
		ID:         article2UID,
		Title:      "Foobar 2",
		Slug:       "foobar-2",
		Content:    "body article 2",
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
		Status:     domain.PUBLISHED,
		UserID:     article2UserID,
		Categories: []string{},
	}

	storage := storage.ArticleStorage{
		ArticleMap: []domain.Article{
			article1, article2,
		},
	}

	query := NewArticleQueryInMemory(&storage)
	result := repository.QueryResult{}

	// When
	go func() {
		result = <-query.FindArticleDetailByID(article1UID)
		done <- true
	}()

	<-done

	// Then
	assert.Equal(t, article1, result.Result.(domain.Article))
}

func TestCanFoundDetailArticleBySlug(t *testing.T) {
	// Given
	done := make(chan bool)

	article1UID := uuid.NewV4()
	article1UserID, _ := uuid.FromString("00000000-0000-0000-0000-000000000001")
	article1 := domain.Article{
		ID:         article1UID,
		Title:      "Foobar",
		Slug:       "foobar",
		Content:    "body article",
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
		Status:     domain.PUBLISHED,
		UserID:     article1UserID,
		Categories: []string{"News", "Politic"},
	}

	article2UID := uuid.NewV4()
	article2UserID, _ := uuid.FromString("00000000-0000-0000-0000-000000000002")
	article2 := domain.Article{
		ID:         article2UID,
		Title:      "Foobar 2",
		Slug:       "foobar-2",
		Content:    "body article 2",
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
		Status:     domain.PUBLISHED,
		UserID:     article2UserID,
		Categories: []string{},
	}

	storage := storage.ArticleStorage{
		ArticleMap: []domain.Article{
			article1, article2,
		},
	}

	query := NewArticleQueryInMemory(&storage)
	result := repository.QueryResult{}

	// When
	go func() {
		result = <-query.FindArticleDetailBySlug(article1.Slug)
		done <- true
	}()

	<-done

	// Then
	assert.Equal(t, article1, result.Result.(domain.Article))
}

func TestCanFoundArticleByStatus(t *testing.T) {
	// Given
	done := make(chan bool)

	article1UID := uuid.NewV4()
	article1UserID, _ := uuid.FromString("00000000-0000-0000-0000-000000000001")
	article1 := domain.Article{
		ID:         article1UID,
		Title:      "Foobar",
		Slug:       "foobar",
		Content:    "body article",
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
		Status:     domain.DRAFT,
		UserID:     article1UserID,
		Categories: []string{"News", "Politic"},
	}

	article2UID := uuid.NewV4()
	article2UserID, _ := uuid.FromString("00000000-0000-0000-0000-000000000002")
	article2 := domain.Article{
		ID:         article2UID,
		Title:      "Foobar 2",
		Slug:       "foobar-2",
		Content:    "body article 2",
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
		Status:     domain.PUBLISHED,
		UserID:     article2UserID,
		Categories: []string{},
	}

	article3UID := uuid.NewV4()
	article3UserID, _ := uuid.FromString("00000000-0000-0000-0000-000000000003")
	article3 := domain.Article{
		ID:         article3UID,
		Title:      "Foobar 3",
		Slug:       "foobar-3",
		Content:    "body article 3",
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
		Status:     domain.PUBLISHED,
		UserID:     article3UserID,
		Categories: []string{},
	}

	storageArticle := storage.ArticleStorage{
		ArticleMap: []domain.Article{
			article1, article2, article3,
		},
	}

	expectResult := []domain.Article{
		article2, article3,
	}

	query := NewArticleQueryInMemory(&storageArticle)
	result := repository.QueryResult{}

	// When
	go func() {
		result = <-query.FindArticlesByStatus(domain.PUBLISHED)
		done <- true
	}()

	<-done

	// Then
	assert.Equal(t, expectResult, result.Result.([]domain.Article))
}

func TestCanFoundArticleByCategory(t *testing.T) {
	// Given
	done := make(chan bool)

	article1UID := uuid.NewV4()
	article1UserID, _ := uuid.FromString("00000000-0000-0000-0000-000000000001")
	article1 := domain.Article{
		ID:         article1UID,
		Title:      "Foobar",
		Slug:       "foobar",
		Content:    "body article",
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
		Status:     domain.DRAFT,
		UserID:     article1UserID,
		Categories: []string{"NEWS", "POLITIK"},
	}

	article2UID := uuid.NewV4()
	article2UserID, _ := uuid.FromString("00000000-0000-0000-0000-000000000002")
	article2 := domain.Article{
		ID:         article2UID,
		Title:      "Foobar 2",
		Slug:       "foobar-2",
		Content:    "body article 2",
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
		Status:     domain.PUBLISHED,
		UserID:     article2UserID,
		Categories: []string{"ENTERTAINMENT"},
	}

	article3UID := uuid.NewV4()
	article3UserID, _ := uuid.FromString("00000000-0000-0000-0000-000000000003")
	article3 := domain.Article{
		ID:         article3UID,
		Title:      "Foobar 3",
		Slug:       "foobar-3",
		Content:    "body article 3",
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
		Status:     domain.PUBLISHED,
		UserID:     article3UserID,
		Categories: []string{"NEWS"},
	}

	storageArticle := storage.ArticleStorage{
		ArticleMap: []domain.Article{
			article1, article2, article3,
		},
	}

	expectResult := []domain.Article{
		article1, article3,
	}

	query := NewArticleQueryInMemory(&storageArticle)
	result := repository.QueryResult{}

	// When
	go func() {
		result = <-query.FindArticlesByCategory("NEWS")
		done <- true
	}()

	<-done

	// Then
	assert.Equal(t, expectResult, result.Result.([]domain.Article))
}
