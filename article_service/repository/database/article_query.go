package database

import (
	"article_service/domain"
	"article_service/repository"
	"database/sql"
	"errors"
	"strings"
	"time"

	"github.com/satori/uuid"
	"github.com/sirupsen/logrus"
)

type ArticleQueryDatabase struct {
	database *sql.DB
}

func NewArticleQueryDatabase(database *sql.DB) repository.ArticleQuery {
	return &ArticleQueryDatabase{database}
}

func parseRow(row *sql.Row) (result repository.QueryResult) {
	var (
		id         uuid.UUID
		slug       string
		title      string
		content    string
		status     string
		userID     uuid.UUID
		createdAt  time.Time
		updatedAt  time.Time
		categories string
		thumbnail  string
	)
	err := row.Scan(&id, &slug, &title, &content, &status, &userID, &createdAt, &updatedAt, &categories, &thumbnail)
	if err != nil {
		result.Error = err
		return
	}
	categories = strings.Replace(categories, "{", "", -1)
	categories = strings.Replace(categories, "}", "", -1)
	splittedCategory := strings.Split(categories, ",")

	newArticle := domain.Article{
		id,
		slug,
		title,
		content,
		status,
		userID,
		createdAt,
		updatedAt,
		splittedCategory,
		thumbnail,
	}

	return repository.QueryResult{newArticle, nil}
}

func parseRowCategory(row *sql.Row) (result repository.QueryResult) {
	var (
		userID   uuid.UUID
		category string
		slug     string
	)
	err := row.Scan(&userID, &category, &slug)
	if err != nil {
		result.Error = err
		return
	}
	newCategory := domain.UserCategory{
		userID,
		category,
		slug,
	}

	return repository.QueryResult{newCategory, nil}
}

func (query *ArticleQueryDatabase) FindAllArticles(offset, limit int) repository.QueryResult {

	rows, err := query.database.Query(
		`SELECT * FROM "Article" ORDER BY updated_at DESC LIMIT $1 OFFSET $2`,
		limit, offset)
	if err != nil {
		result := repository.QueryResult{nil, err}
		return result
	}
	result := parseRows(rows)

	return result

}

func parseRows(rows *sql.Rows) (result repository.QueryResult) {
	var articles []domain.Article
	for rows.Next() {
		var (
			id         uuid.UUID
			slug       string
			title      string
			content    string
			status     string
			userID     uuid.UUID
			createdAt  time.Time
			updatedAt  time.Time
			categories string
			thumbnail  string
		)
		err := rows.Scan(&id, &slug, &title, &content, &status, &userID, &createdAt, &updatedAt, &categories, &thumbnail)
		if err != nil {
			result.Error = err
			return
		}

		categories = strings.Replace(categories, "{", "", -1)
		categories = strings.Replace(categories, "}", "", -1)
		splittedCategory := strings.Split(categories, ",")

		newArticle := domain.Article{
			id,
			slug,
			title,
			content,
			status,
			userID,
			createdAt,
			updatedAt,
			splittedCategory,
			thumbnail,
		}

		articles = append(articles, newArticle)
	}

	result.Result = articles

	return
}

func parseRowsCategory(rows *sql.Rows) (result repository.QueryResult) {
	var categories []domain.UserCategory
	for rows.Next() {
		var (
			userID   uuid.UUID
			category string
			slug     string
		)
		err := rows.Scan(&userID, &category, &slug)
		if err != nil {
			result.Error = err
			return
		}

		newCategory := domain.UserCategory{
			userID,
			category,
			slug,
		}
		categories = append(categories, newCategory)
	}

	result.Result = categories

	return
}

func (query *ArticleQueryDatabase) FindArticlesByStatus(status string, offset, limit int) repository.QueryResult {

	rows, err := query.database.Query(
		`SELECT * FROM "Article" WHERE status = $1 ORDER BY updated_at DESC LIMIT $2 OFFSET $3`, status, limit, offset)
	if err != nil {
		result := repository.QueryResult{nil, err}
		return result
	}

	result := parseRows(rows)

	return result
}

func (query *ArticleQueryDatabase) FindArticleDetailByID(id uuid.UUID) repository.QueryResult {

	row := query.database.QueryRow(
		`SELECT * FROM "Article" WHERE id = $1 `, id)
	result := parseRow(row)

	return result
}

func (query *ArticleQueryDatabase) FindArticlesByUserId(id uuid.UUID, offset, limit int) repository.QueryResult {

	rows, err := query.database.Query(
		`SELECT * FROM "Article" WHERE user_id = $1 ORDER BY updated_at DESC LIMIT $2 OFFSET $3`, id, limit, offset)
	if err != nil {
		result := repository.QueryResult{nil, err}
		return result
	}
	result := parseRows(rows)
	logrus.Println(result)
	return result
}

func (query *ArticleQueryDatabase) FindArticleDetailBySlug(slug string) repository.QueryResult {

	row := query.database.QueryRow(
		`SELECT * FROM "Article" WHERE slug = $1`, slug)
	if row == nil {
		result := repository.QueryResult{nil, errors.New(" no article found")}
		return result
	}
	result := parseRow(row)

	return result
}

func (query *ArticleQueryDatabase) FindArticlesByCategory(category string, offset, limit int) repository.QueryResult {

	rows, err := query.database.Query(
		`SELECT * FROM "Article" WHERE array_to_string(categories, ',') LIKE '%' || $1 || '%' ORDER BY updated_at DESC LIMIT $2 OFFSET $3`, category, limit, offset)
	if err != nil {
		result := repository.QueryResult{nil, err}
		return result
	}
	result := parseRows(rows)

	return result
}

func (query *ArticleQueryDatabase) FindCategoriesByUserId(id uuid.UUID, offset, limit int) repository.QueryResult {

	rows, err := query.database.Query(
		`SELECT * FROM "Category" WHERE user_id = $1 LIMIT $2 OFFSET $3`,
		id,
		limit,
		offset)
	if err != nil {
		result := repository.QueryResult{nil, err}
		return result
	}
	result := parseRowsCategory(rows)

	return result
}

func (query *ArticleQueryDatabase) FindCategoriesBySlug(slug string) repository.QueryResult {

	rows, err := query.database.Query(
		`SELECT * FROM "Category" WHERE slug = $1`, slug)
	if err != nil {
		result := repository.QueryResult{nil, err}
		return result
	}
	result := parseRowsCategory(rows)

	return result
}

func (query *ArticleQueryDatabase) Search(keyword string, offset, limit int) repository.QueryResult {

	rows, err := query.database.Query(
		`SELECT * FROM "Article" WHERE title LIKE '%' || $1 || '%' ORDER BY updated_at DESC LIMIT $2 OFFSET $3`, keyword, limit, offset)
	if err != nil {
		result := repository.QueryResult{nil, err}
		return result
	}

	result := parseRows(rows)

	return result
}
