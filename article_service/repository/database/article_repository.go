package database

import (
	"article_service/domain"
	"article_service/repository"
	"database/sql"
	"errors"
	"log"

	"github.com/sirupsen/logrus"
)

type ArticleRepositoryDatabase struct {
	database *sql.DB
}

func NewArticleRepositoryDatabase(db *sql.DB) repository.ArticleRepository {
	return &ArticleRepositoryDatabase{db}
}

func (repository *ArticleRepositoryDatabase) Save(article *domain.Article) (err error) {

	categories := "{"
	if len(article.Categories) > 0 {
		for _, category := range article.Categories {
			categories += category + ","
		}
		categories = categories[:len(categories)-1]
	}
	categories += "}"
	_, execErr := repository.database.Exec(
		`INSERT INTO "Article" VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)`,
		article.ID,
		article.Slug,
		article.Title,
		article.Content,
		article.Status,
		article.UserID,
		article.CreatedAt,
		article.UpdatedAt,
		categories,
		article.Thumbnail)
	err = execErr

	return err
}

func (repository *ArticleRepositoryDatabase) IsSlugExist(slug string) bool {
	row := repository.database.QueryRow(`
			SELECT * FROM "User" WHERE slug = $1;
		`, slug)

	if row == nil {
		return false
	}

	return true
}

func (repository *ArticleRepositoryDatabase) SaveCategory(category *domain.UserCategory) (err error) {

	if repository.IsCategoryExist(category.Name, category.Slug) == true {
		err = errors.New("Category is exist")
	} else if category.Name == "NEWS" || category.Name == "OTOMOTIF" || category.Name == "ENTERTAINMENT" || category.Name == "POLITIK" {
		err = errors.New("Can't add default category. Category is exist")
	} else {
		_, execErr := repository.database.Exec(
			`INSERT INTO "Category" VALUES($1, $2, $3)`,
			category.UserID,
			category.Name,
			category.Slug)
		err = execErr
	}

	return err
}

func (repository *ArticleRepositoryDatabase) IsCategoryExist(category string, slug string) bool {
	var count int
	row := repository.database.QueryRow(`
			SELECT COUNT(*) FROM "Category" WHERE name = $1 AND slug = $2;
		`, category, slug).Scan(&count)

	switch {
	case row != nil:
		log.Fatal(row)
	default:
		logrus.Println("Number of rows are", count)
	}

	if count > 0 {
		return true
	}

	return false
}

func (repository *ArticleRepositoryDatabase) Update(article *domain.Article) (err error) {

	categories := "{"
	if len(article.Categories) > 0 {
		for _, category := range article.Categories {
			categories += category + ","
		}
		categories = categories[:len(categories)-1]
	}
	categories += "}"
	_, execErr := repository.database.Exec(
		`UPDATE "Article" SET slug = $1, title = $2, content = $3, status = $4, updated_at = $5, categories = $6, thumbnail = $7 WHERE id = $8`,
		article.Slug,
		article.Title,
		article.Content,
		article.Status,
		article.UpdatedAt,
		categories,
		article.Thumbnail,
		article.ID)
	err = execErr

	return err
}
