package database

import (
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/satori/uuid"
	"github.com/stretchr/testify/assert"
)

func TestCanFindArticleBySlug(t *testing.T) {
	columns := []string{"ID", "SLUG", "TITLE", "CONTENT", "STATUS", "USER_ID", "CREATED_AT", "UPDATED_AT", "CATEGORIES", "THUMBNAIL"}

	id, _ := uuid.FromString("77081330-896f-4d9e-b1d3-d4da2eb5f896")
	slug := "matahari"
	category := []string{"News", "Politic"}
	categories := "{"
	if len(category) > 0 {
		for _, category := range category {
			categories += category + ","
		}
		categories = categories[:len(categories)-1]
	}
	categories += "}"
	db, mock, _ := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	mock.ExpectQuery(`SELECT * FROM "Article" WHERE slug = $1`).
		WithArgs(slug).
		WillReturnRows(
			sqlmock.NewRows(columns).AddRow(
				id,
				slug,
				"Title Matahari",
				"Content MAtahari",
				"PUBLISHED",
				id,
				time.Now(),
				time.Now(),
				categories,
				"https://google.com",
			),
		)

	repo := NewArticleQueryDatabase(db)

	result := repo.FindArticleDetailBySlug(slug)

	assert.Nil(t, result.Error)
	assert.NotEmpty(t, result.Result)

}

func TestCanFindArticleByID(t *testing.T) {
	columns := []string{"ID", "SLUG", "TITLE", "CONTENT", "STATUS", "USER_ID", "CREATED_AT", "UPDATED_AT", "CATEGORIES", "THUMBNAIL"}

	id, _ := uuid.FromString("77081330-896f-4d9e-b1d3-d4da2eb5f896")
	slug := "matahari"
	category := []string{"News", "Politic"}
	categories := "{"
	if len(category) > 0 {
		for _, category := range category {
			categories += category + ","
		}
		categories = categories[:len(categories)-1]
	}
	categories += "}"
	db, mock, _ := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	mock.ExpectQuery(`SELECT * FROM "Article" WHERE id = $1`).
		WithArgs(id).
		WillReturnRows(
			sqlmock.NewRows(columns).AddRow(
				id,
				slug,
				"Title Matahari",
				"Content MAtahari",
				"PUBLISHED",
				id,
				time.Now(),
				time.Now(),
				categories,
				"https://google.com",
			),
		)

	repo := NewArticleQueryDatabase(db)

	result := repo.FindArticleDetailByID(id)

	assert.Nil(t, result.Error)
	assert.NotEmpty(t, result.Result)

}

func TestCanFindAllArticle(t *testing.T) {
	columns := []string{"ID", "SLUG", "TITLE", "CONTENT", "STATUS", "USER_ID", "CREATED_AT", "UPDATED_AT", "CATEGORIES", "THUMBNAIL"}

	id, _ := uuid.FromString("77081330-896f-4d9e-b1d3-d4da2eb5f896")
	slug := "matahari"
	category := []string{"News", "Politic"}
	categories := "{"
	if len(category) > 0 {
		for _, category := range category {
			categories += category + ","
		}
		categories = categories[:len(categories)-1]
	}
	categories += "}"
	db, mock, _ := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	mock.ExpectQuery(`SELECT * FROM "Article" ORDER BY updated_at DESC LIMIT $1 OFFSET $2`).
		WithArgs(5, 0).
		WillReturnRows(
			sqlmock.NewRows(columns).AddRow(
				id,
				slug,
				"Title Matahari",
				"Content MAtahari",
				"PUBLISHED",
				id,
				time.Now(),
				time.Now(),
				categories,
				"https://google.com",
			),
		)

	repo := NewArticleQueryDatabase(db)

	result := repo.FindAllArticles(0, 5)

	assert.Nil(t, result.Error)
	assert.NotEmpty(t, result.Result)

}
