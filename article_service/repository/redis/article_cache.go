package redis

import (
	"article_service/domain"
	"article_service/repository"
	"encoding/json"

	"github.com/gomodule/redigo/redis"
)

type RedisCache struct {
	pool *redis.Pool
}

func New(pool *redis.Pool) repository.ArticleCache {
	return &RedisCache{
		pool: pool,
	}
}

func (r *RedisCache) Save(article domain.Article) error {
	j, err := json.Marshal(article)
	if err != nil {
		return err
	}

	c := r.pool.Get()
	defer c.Close()

	_, err = c.Do("SET", article.Slug, j)
	if err != nil {
		return err
	}

	return nil
}

func (r *RedisCache) Get(slug string) (domain.Article, error) {
	c := r.pool.Get()
	defer c.Close()

	j, err := c.Do("GET", slug)
	if err != nil {
		return domain.Article{}, err
	}
	if j == nil {
		return domain.Article{}, nil
	}

	article := domain.Article{}
	err = json.Unmarshal(j.([]byte), &article)
	if err != nil && err != redis.ErrNil {
		return domain.Article{}, err
	}

	return article, nil
}
