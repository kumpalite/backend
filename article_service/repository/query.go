package repository

import (
	"article_service/domain"

	"github.com/satori/uuid"
)

// QueryResult is to wrap query result
type QueryResult struct {
	Result interface{}
	Error  error
}

// ArticleQuery is contract for article query
type ArticleQuery interface {
	// single
	FindAllArticles(offset, limit int) QueryResult
	FindArticleDetailByID(id uuid.UUID) QueryResult
	FindArticleDetailBySlug(slug string) QueryResult
	FindArticlesByStatus(status string, offset, limit int) QueryResult
	FindArticlesByCategory(category string, offset, limit int) QueryResult
	FindArticlesByUserId(id uuid.UUID, offset, limit int) QueryResult
	FindCategoriesByUserId(id uuid.UUID, offset, limit int) QueryResult
	FindCategoriesBySlug(slug string) QueryResult
	Search(keyword string, offset, limit int) QueryResult
}

type ArticleCache interface {
	Save(Article domain.Article) error
	Get(slug string) (domain.Article, error)
}
