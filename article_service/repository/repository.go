package repository

import "article_service/domain"

// ArticleRepository is wrap contract article repository
type ArticleRepository interface {
	Save(article *domain.Article) error
	SaveCategory(category *domain.UserCategory) error
	IsSlugExist(slug string) bool
	IsCategoryExist(category, slug string) bool
	Update(article *domain.Article) error
}
