package storage

import (
	"article_service/domain"
)

// ArticleStorage is article storage in memory
type ArticleStorage struct {
	ArticleMap  []domain.Article
	CategoryMap []domain.UserCategory
}

// NewArticleStorage is to create article storage
func NewArticleStorage() *ArticleStorage {
	return &ArticleStorage{
		ArticleMap:  []domain.Article{},
		CategoryMap: []domain.UserCategory{},
	}
}
