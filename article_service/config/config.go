package config

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

type Config struct {
	PORT          string
	DATABASE_ADDR string
	REDIS_ADDR    string
}

func Load(mode string) Config {
	if mode == "dev" {
		err := godotenv.Load(".env")
		if err != nil {
			log.Println(err)
		}
	}

	PORT := os.Getenv("ARTICLE_SERVICE_PORT")
	DATABASE_ADDR := os.Getenv("DATABASE_ADDR")
	REDIS_ADDR := os.Getenv("REDIS_ADDR")

	if PORT == "" {
		PORT = ":9000"
	}

	return Config{
		PORT:          PORT,
		REDIS_ADDR:    REDIS_ADDR,
		DATABASE_ADDR: DATABASE_ADDR,
	}
}
