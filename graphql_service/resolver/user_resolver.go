package resolver

import (
	"context"
	"graphql_service/structure"

	"github.com/graph-gophers/graphql-go"
)

type UserResolver struct {
	meta structure.User
}

func (resolver *UserResolver) ID(context context.Context) graphql.ID {
	return graphql.ID(resolver.meta.Id.String())
}

func (resolver *UserResolver) Username(context context.Context) string {
	return resolver.meta.Username
}

func (resolver *UserResolver) Fullname(context context.Context) string {
	return resolver.meta.Fullname
}

func (resolver *UserResolver) Email(context context.Context) string {
	return resolver.meta.Email
}

func (resolver *UserResolver) Password(context context.Context) string {
	return resolver.meta.Password
}
