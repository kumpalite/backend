package resolver

import (
	"context"
	pb "graphql_service/proto"
	"graphql_service/structure"

	"github.com/satori/uuid"

	"github.com/graph-gophers/graphql-go"
)

type ArticleResolver struct {
	meta     structure.Article
	author   structure.User
	comments []CommentResolver
	category structure.Category
}

func (resolver *ArticleResolver) Id() graphql.ID {
	return graphql.ID(resolver.meta.Id.String())
}

func (resolver *ArticleResolver) Title() string {
	return resolver.meta.Title
}

func (resolver *ArticleResolver) CreatedAt() string {
	return resolver.meta.CreatedAt
}

func (resolver *ArticleResolver) UpdatedAt() string {
	return resolver.meta.UpdatedAt
}

func (resolver *ArticleResolver) Slug() string {
	return resolver.meta.Slug
}

func (resolver *ArticleResolver) Content() string {
	return resolver.meta.Content
}

func (resolver *ArticleResolver) Status() string {
	return resolver.meta.Status
}

func (resolver *ArticleResolver) Category() []*string {
	var result []*string
	for i, _ := range resolver.meta.Category {
		result = append(result, &resolver.meta.Category[i])
	}
	return result
}

func (resolver *ArticleResolver) IdAuthor() graphql.ID {
	return graphql.ID(resolver.meta.IdAuthor.String())
}

func (resolver *ArticleResolver) Author() *UserResolver {
	author, err := ServiceConnection.UserService.GetUserById(context.Background(), &pb.UserRequest{
		Id: resolver.meta.IdAuthor.String(),
	})
	if err != nil {
		return nil
	}

	authorID, err := uuid.FromString(author.Id)
	if err != nil {
		return nil
	}

	result := structure.User{
		Id:       authorID,
		Email:    author.Email,
		Username: author.Username,
		Fullname: author.Fullname,
	}

	return &UserResolver{
		meta: result,
	}
}

func (resolver *ArticleResolver) Comments() *[]*CommentResolver {
	var results = make([]*CommentResolver, len(resolver.comments))

	for k, v := range resolver.comments {
		results[k] = &CommentResolver{
			meta:   v.meta,
			author: v.author,
		}
	}
	return &results
}

func (resolver *ArticleResolver) TotalComment() (int32, error) {
	totalComment, err := ServiceConnection.CommentService.CountAllCommentByArticleId(context.Background(), &pb.CommentRequest{
		IdArticle: resolver.meta.Id.String(),
	})
	if err != nil {
		return 0, err
	}
	return totalComment.Total, nil
}

func (resolver *ArticleResolver) Username() string {
	return resolver.author.Username
}

func (resolver *ArticleResolver) Name() string {
	return resolver.category.Name
}

func (resolver *ArticleResolver) Thumbnail() string {
	return resolver.meta.Thumbnail
}

func (resolver *ArticleResolver) SlugCategory() string {
	return resolver.category.SlugCategory
}
