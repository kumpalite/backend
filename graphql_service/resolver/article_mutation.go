package resolver

import (
	"context"
	"encoding/json"
	pb "graphql_service/proto"
	"graphql_service/structure"
	"regexp"

	"github.com/graph-gophers/graphql-go"
	"github.com/satori/uuid"
)

type ArticleRequest struct {
	Title     string
	Slug      string
	Content   string
	Status    string
	Category  []*string
	Thumbnail *string
}

type ArticleUpdateRequest struct {
	IdArticle graphql.ID
	Title     string
	Slug      string
	Content   string
	Status    string
	Category  []*string
	Thumbnail *string
}

type CategoryRequest struct {
	Name         string
	SlugCategory string
}

func (resolver *Resolver) CreateArticle(context context.Context, params struct{ Article *ArticleRequest }) (*ArticleResolver, error) {

	args := params.Article

	userId := context.Value("user").(string)

	var categoriesRequest []*pb.Category
	for _, c := range args.Category {
		categoriesRequest = append(categoriesRequest, &pb.Category{
			Value: *c,
		})
	}

	regexPattern := `{"src":"[^"}]*"}`
	var thumbnail string
	if params.Article.Thumbnail == nil {
		thumbnail = "https://wildwestcomms.co.uk/wp-content/uploads/2016/01/grey-placeholder.png"
		match, _ := regexp.MatchString(regexPattern, args.Content)
		if match {
			pattern, _ := regexp.Compile(regexPattern)
			thumbnailJSON := pattern.FindString(args.Content)
			var dat map[string]interface{}
			json.Unmarshal([]byte(thumbnailJSON), &dat)
			thumbnailData, ok := dat["src"]
			if ok {
				thumbnail = thumbnailData.(string)
			}
		}
	} else {
		thumbnail = *params.Article.Thumbnail
	}
	response, err := resolver.ArticleService.CreateArticle(context, &pb.ArticleRequest{
		Title:      args.Title,
		Slug:       args.Slug,
		Content:    args.Content,
		Status:     args.Status,
		Categories: categoriesRequest,
		UserId:     userId,
		Thumbnail:  thumbnail,
	})

	if err != nil {
		return nil, err
	}

	uid, err := uuid.FromString(response.Id)
	if err != nil {
		return nil, err
	}

	author, err := resolver.UserService.GetUserById(context, &pb.UserRequest{Id: userId})
	if err != nil {
		return nil, err
	}

	authorUid, err := uuid.FromString(userId)
	if err != nil {
		return nil, err
	}

	return &ArticleResolver{
		meta: structure.Article{
			Id:        uid,
			Title:     response.Title,
			CreatedAt: response.CreatedAt,
			UpdatedAt: response.UpdatedAt,
			Slug:      response.Slug,
			Content:   response.Content,
			Status:    response.Status,
			Thumbnail: response.Thumbnail,
		},
		author: structure.User{
			Id:       authorUid,
			Fullname: author.Fullname,
			Email:    author.Email,
			Username: author.Username,
		},
	}, nil
}

func (resolver *Resolver) CreateCategory(context context.Context, params struct{ Category *CategoryRequest }) (*ArticleResolver, error) {
	args := params.Category

	userId := context.Value("user").(string)

	response, err := resolver.ArticleService.CreateCategory(context, &pb.Category{
		UserId: userId,
		Value:  args.Name,
		Slug:   args.SlugCategory,
	})
	if err != nil {
		return nil, err
	}

	author, err := resolver.UserService.GetUserById(context, &pb.UserRequest{Id: userId})
	if err != nil {
		return nil, err
	}

	authorID, _ := uuid.FromString(userId)
	if err != nil {
		return nil, err
	}

	return &ArticleResolver{
		category: structure.Category{
			Name:         response.Value,
			SlugCategory: response.Slug,
		},
		author: structure.User{
			Id:       authorID,
			Fullname: author.Fullname,
			Email:    author.Email,
			Username: author.Username,
		},
	}, nil
}

func (resolver *Resolver) UpdateArticle(context context.Context, params struct{ Article *ArticleUpdateRequest }) (*ArticleResolver, error) {

	args := params.Article

	userId := context.Value("user").(string)

	var categoriesRequest []*pb.Category
	for _, c := range args.Category {
		categoriesRequest = append(categoriesRequest, &pb.Category{
			Value: *c,
		})
	}

	regexPattern := `{"src":"[^"}]*"}`
	var thumbnail string
	if params.Article.Thumbnail == nil {
		thumbnail = "https://wildwestcomms.co.uk/wp-content/uploads/2016/01/grey-placeholder.png"
		match, _ := regexp.MatchString(regexPattern, args.Content)
		if match {
			pattern, _ := regexp.Compile(regexPattern)
			thumbnailJSON := pattern.FindString(args.Content)
			var dat map[string]interface{}
			json.Unmarshal([]byte(thumbnailJSON), &dat)
			thumbnailData, ok := dat["src"]
			if ok {
				thumbnail = thumbnailData.(string)
			}
		}
	} else {
		thumbnail = *params.Article.Thumbnail
	}
	response, err := resolver.ArticleService.UpdateArticle(context, &pb.ArticleRequest{
		Id:         string(args.IdArticle),
		Title:      args.Title,
		Slug:       args.Slug,
		Content:    args.Content,
		Status:     args.Status,
		Categories: categoriesRequest,
		UserId:     userId,
		Thumbnail:  thumbnail,
	})

	if err != nil {
		return nil, err
	}

	uid, err := uuid.FromString(response.Id)
	if err != nil {
		return nil, err
	}

	author, err := resolver.UserService.GetUserById(context, &pb.UserRequest{Id: userId})
	if err != nil {
		return nil, err
	}

	authorUid, err := uuid.FromString(userId)
	if err != nil {
		return nil, err
	}

	return &ArticleResolver{
		meta: structure.Article{
			Id:        uid,
			Title:     response.Title,
			CreatedAt: response.CreatedAt,
			UpdatedAt: response.UpdatedAt,
			Slug:      response.Slug,
			Content:   response.Content,
			Status:    response.Status,
			Thumbnail: response.Thumbnail,
		},
		author: structure.User{
			Id:       authorUid,
			Fullname: author.Fullname,
			Email:    author.Email,
			Username: author.Username,
		},
	}, nil
}
