package resolver

import (
	"context"
	pb "graphql_service/proto"
	"graphql_service/structure"

	"github.com/graph-gophers/graphql-go"
	"github.com/satori/uuid"
)

type CommentResolver struct {
	meta   structure.Comment
	author structure.User
}

func (resolver *CommentResolver) ID(context context.Context) graphql.ID {
	return graphql.ID(resolver.meta.Id.String())
}

func (resolver *CommentResolver) Content(context context.Context) string {
	return resolver.meta.Content
}

func (resolver *CommentResolver) IdAuthor(context context.Context) graphql.ID {
	return graphql.ID(resolver.meta.IdAuthor.String())
}

func (resolver *CommentResolver) IdArticle(context context.Context) graphql.ID {
	return graphql.ID(resolver.meta.IdArticle.String())
}

func (resolver *CommentResolver) Author() *UserResolver {
	author, err := ServiceConnection.UserService.GetUserById(context.Background(), &pb.UserRequest{
		Id: resolver.meta.IdAuthor.String(),
	})
	if err != nil {
		return nil
	}

	authorID, err := uuid.FromString(author.Id)
	if err != nil {
		return nil
	}

	result := structure.User{
		Id:       authorID,
		Email:    author.Email,
		Username: author.Username,
		Fullname: author.Fullname,
	}

	return &UserResolver{
		meta: result,
	}
}
