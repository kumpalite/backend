package resolver

import (
	"context"
	pb "graphql_service/proto"
	"graphql_service/structure"

	"github.com/graph-gophers/graphql-go"
	"github.com/satori/uuid"
)

type CommentRequest struct {
	Content   string
	IdArticle graphql.ID
}

func (resolver *Resolver) CreateComment(context context.Context, params struct{ Comment *CommentRequest }) (*CommentResolver, error) {
	args := params.Comment

	authorId := context.Value("user").(string)
	response, err := resolver.CommentService.CreateComment(context, &pb.CommentRequest{
		IdUser:    authorId,
		IdArticle: string(args.IdArticle),
		Content:   args.Content,
	})

	if err != nil {
		return nil, err
	}

	uid, err := uuid.FromString(response.Id)
	if err != nil {
		return nil, err
	}
	author, err := resolver.UserService.GetUserById(context, &pb.UserRequest{Id: response.IdUser})

	if err != nil {
		return nil, err
	}

	authorUUID, _ := uuid.FromString(response.IdUser)
	articleUUID, _ := uuid.FromString(response.IdUser)

	return &CommentResolver{
		meta: structure.Comment{
			Id:        uid,
			IdAuthor:  authorUUID,
			IdArticle: articleUUID,
			Content:   response.Content,
		},
		author: structure.User{
			Id:       authorUUID,
			Fullname: author.Fullname,
			Email:    author.Email,
			Username: author.Username,
		},
	}, nil
}
