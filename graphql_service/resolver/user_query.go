package resolver

import (
	"context"
	pb "graphql_service/proto"
	"graphql_service/structure"

	"github.com/graph-gophers/graphql-go"
	"github.com/satori/uuid"
)

func (resolver *Resolver) GetUserById(context context.Context, args struct{ ID graphql.ID }) (result *UserResolver, err error) {
	response, err := resolver.UserService.GetUserById(context, &pb.UserRequest{
		Id: string(args.ID),
	})

	if err != nil {
		return nil, err
	}

	uid, err := uuid.FromString(response.Id)
	if err != nil {
		return nil, err
	}

	return &UserResolver{
		meta: structure.User{
			Id:       uid,
			Fullname: response.Fullname,
			Username: response.Username,
			Password: response.Password,
			Email:    response.Email,
		},
	}, nil
}

func (resolver *Resolver) GetUserByUsername(context context.Context, args struct{ Username string }) (result *UserResolver, err error) {
	response, err := resolver.UserService.GetUserByUsername(context, &pb.UserRequest{
		Username: args.Username,
	})

	if err != nil {
		return nil, err
	}

	uid, err := uuid.FromString(response.Id)
	if err != nil {
		return nil, err
	}

	return &UserResolver{
		meta: structure.User{
			Id:       uid,
			Fullname: response.Fullname,
			Username: response.Username,
			Password: response.Password,
			Email:    response.Email,
		},
	}, nil
}

func (resolver *Resolver) GetUser(context context.Context, args struct {
	Username *string
	ID       *graphql.ID
}) (result *UserResolver, err error) {
	if args.Username != nil {
		type usernameArgs struct{ Username string }
		return resolver.GetUserByUsername(context, usernameArgs{*args.Username})
	}

	type idArgs struct{ ID graphql.ID }
	if args.ID != nil {
		return resolver.GetUserById(context, idArgs{*args.ID})
	}

	userId := context.Value("user").(string)
	return resolver.GetUserById(context, idArgs{graphql.ID(userId)})
}
