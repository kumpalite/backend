package resolver

import (
	"context"

	pb "graphql_service/proto"
	"graphql_service/structure"

	"github.com/graph-gophers/graphql-go"
	"github.com/satori/uuid"
)

func (resolver *Resolver) CountAllCommentByArticleId(context context.Context, args struct{ ArticleID graphql.ID }) (result int32, err error) {
	response, err := resolver.CommentService.CountAllCommentByArticleId(context, &pb.CommentRequest{
		IdArticle: string(args.ArticleID),
	})
	if err != nil {
		return 0, err
	}

	return response.Total, nil
}

func (resolver *Resolver) GetCommentByArticleId(context context.Context, args struct {
	ArticleID graphql.ID
	Limit     int32
	Offset    int32
}) (*[]*CommentResolver, error) {
	response, err := resolver.CommentService.GetCommentByArticleId(context, &pb.CommentRequest{
		IdArticle: string(args.ArticleID),
		Limit:     args.Limit,
		Offset:    args.Offset,
	})

	if err != nil {
		return nil, err
	}

	comments := response.CommentList
	if err != nil {
		return nil, err
	}

	result := make([]*CommentResolver, len(comments))

	for i, v := range comments {
		uid, err := uuid.FromString(v.Id)
		if err != nil {
			return nil, err
		}

		authorId, err := uuid.FromString(v.IdUser)
		if err != nil {
			return nil, err
		}
		result[i] = &CommentResolver{
			meta: structure.Comment{
				Id:       uid,
				Content:  v.Content,
				IdAuthor: authorId,
			},
		}
	}
	return &result, nil
}

func (resolver *Resolver) GetCommentByArticleSlug(context context.Context, args struct {
	Slug   string
	Limit  int32
	Offset int32
}) (*[]*CommentResolver, error) {
	type slugArgs struct {
		Slug string
	}
	article, err := resolver.GetArticleBySlug(context, slugArgs{args.Slug})
	if err != nil {
		return nil, err
	}

	response, err := resolver.CommentService.GetCommentByArticleId(context, &pb.CommentRequest{
		IdArticle: article.meta.Id.String(),
		Limit:     args.Limit,
		Offset:    args.Offset,
	})

	if err != nil {
		return nil, err
	}

	comments := response.CommentList
	if err != nil {
		return nil, err
	}

	result := make([]*CommentResolver, len(comments))

	for i, v := range comments {
		uid, err := uuid.FromString(v.Id)
		if err != nil {
			return nil, err
		}

		authorId, err := uuid.FromString(v.IdUser)
		if err != nil {
			return nil, err
		}
		result[i] = &CommentResolver{
			meta: structure.Comment{
				Id:       uid,
				Content:  v.Content,
				IdAuthor: authorId,
			},
		}
	}
	return &result, nil
}
