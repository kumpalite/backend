package resolver

import (
	"graphql_service/config"
	service "graphql_service/grpc"
	pb "graphql_service/proto"

	"github.com/sirupsen/logrus"
)

var ServiceConnection *Resolver

type Resolver struct {
	UserService    pb.UserServiceClient
	ArticleService pb.ArticleServiceClient
	CommentService pb.CommentServiceClient
}

func New(config *config.Config) *Resolver {
	userService, err := service.NewUserServiceConnector(*config)
	if err != nil {
		logrus.Fatal(err)
	}

	articleService, err := service.NewArticleServiceConnector(*config)
	if err != nil {
		logrus.Fatal(err)
	}

	commentService, err := service.NewCommentServiceConnector(*config)
	if err != nil {
		logrus.Fatal(err)
	}

	return &Resolver{
		UserService:    userService,
		ArticleService: articleService,
		CommentService: commentService,
	}
}
