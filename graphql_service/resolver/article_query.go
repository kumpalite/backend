package resolver

import (
	"context"
	pb "graphql_service/proto"
	"graphql_service/structure"
	"strings"

	"github.com/graph-gophers/graphql-go"
	"github.com/satori/uuid"
	"github.com/sirupsen/logrus"
)

func (resolver *Resolver) GetArticleBySlug(context context.Context, args struct{ Slug string }) (result *ArticleResolver, err error) {
	response, err := resolver.ArticleService.GetArticleDetailBySlug(context, &pb.ArticleRequest{
		Slug: args.Slug,
	})

	if err != nil {
		return nil, err
	}

	uid, err := uuid.FromString(response.Id)
	if err != nil {
		return nil, err
	}

	var categories []string
	for _, v := range response.Categories {
		categories = append(categories, v.Value)
	}

	authorID, err := uuid.FromString(response.UserId)
	if err != nil {
		return nil, err
	}

	return &ArticleResolver{
		meta: structure.Article{
			Id:        uid,
			Title:     response.Title,
			Slug:      response.Slug,
			Content:   response.Content,
			Status:    response.Status,
			CreatedAt: response.CreatedAt,
			UpdatedAt: response.UpdatedAt,
			Category:  categories,
			Thumbnail: response.Thumbnail,
			IdAuthor:  authorID,
		},
	}, nil
}

func (resolver *Resolver) GetArticleByID(context context.Context, args struct{ ID graphql.ID }) (result *ArticleResolver, err error) {
	response, err := resolver.ArticleService.GetArticleDetailById(context, &pb.ArticleRequest{
		Id: string(args.ID),
	})

	if err != nil {
		return nil, err
	}

	uid, err := uuid.FromString(response.Id)
	if err != nil {
		return nil, err
	}

	var categories []string
	for _, v := range response.Categories {
		categories = append(categories, v.String())
	}

	authorID, err := uuid.FromString(response.UserId)
	if err != nil {
		return nil, err
	}

	return &ArticleResolver{
		meta: structure.Article{
			Id:        uid,
			Title:     response.Title,
			Slug:      response.Slug,
			Content:   response.Content,
			Status:    response.Status,
			CreatedAt: response.CreatedAt,
			UpdatedAt: response.UpdatedAt,
			Category:  categories,
			Thumbnail: response.Thumbnail,
			IdAuthor:  authorID,
		},
	}, nil
}

func (resolver *Resolver) GetArticleByCategory(context context.Context, args struct {
	Category string
	Offset   int32
	Limit    int32
}) (*[]*ArticleResolver, error) {
	response, err := resolver.ArticleService.GetArticleByCategory(context, &pb.Category{
		Value:  args.Category,
		Offset: args.Offset,
		Limit:  args.Limit,
	})

	if err != nil {
		return nil, err
	}

	articles := response.ArticleList
	if err != nil {
		return nil, err
	}

	result := make([]*ArticleResolver, len(articles))

	for i, v := range articles {
		uid, err := uuid.FromString(v.Id)
		if err != nil {
			return nil, err
		}

		var categories []string
		for _, category := range v.Categories {
			categories = append(categories, category.Value)
		}

		authorID, err := uuid.FromString(v.UserId)
		if err != nil {
			return nil, err
		}

		result[i] = &ArticleResolver{
			meta: structure.Article{
				Id:        uid,
				Title:     v.Title,
				Slug:      v.Slug,
				Content:   v.Content,
				Status:    v.Status,
				CreatedAt: v.CreatedAt,
				UpdatedAt: v.UpdatedAt,
				Category:  categories,
				Thumbnail: v.Thumbnail,
				IdAuthor:  authorID,
			},
		}
	}
	return &result, nil
}

func (resolver *Resolver) GetArticleByStatus(context context.Context, args struct {
	Status string
	Limit  int32
	Offset int32
}) (*[]*ArticleResolver, error) {
	response, err := resolver.ArticleService.GetArticleByStatus(context, &pb.ArticleRequest{
		Status: args.Status,
		Offset: args.Offset,
		Limit:  args.Limit,
	})

	logrus.Println(response)

	if err != nil {
		return nil, err
	}

	articles := response.ArticleList
	if err != nil {
		return nil, err
	}

	result := make([]*ArticleResolver, len(articles))

	for i, v := range articles {
		uid, err := uuid.FromString(v.Id)
		if err != nil {
			return nil, err
		}

		var categories []string
		for _, category := range v.Categories {
			categories = append(categories, category.Value)
		}

		authorID, err := uuid.FromString(v.UserId)
		if err != nil {
			return nil, err
		}

		result[i] = &ArticleResolver{
			meta: structure.Article{
				Id:        uid,
				Title:     v.Title,
				Slug:      v.Slug,
				Content:   v.Content,
				Status:    v.Status,
				CreatedAt: v.CreatedAt,
				UpdatedAt: v.UpdatedAt,
				Category:  categories,
				Thumbnail: v.Thumbnail,
				IdAuthor:  authorID,
			},
		}
	}
	return &result, nil
}

func (resolver *Resolver) GetAllArticle(context context.Context, args struct {
	Offset int32
	Limit  int32
}) (*[]*ArticleResolver, error) {
	param := pb.ArticleRequest{
		Limit:  args.Limit,
		Offset: args.Offset,
	}
	response, err := resolver.ArticleService.GetAll(context, &param)

	if err != nil {
		return nil, err
	}

	articles := response.ArticleList
	if err != nil {
		return nil, err
	}

	result := make([]*ArticleResolver, len(articles))

	for i, v := range articles {
		uid, err := uuid.FromString(v.Id)
		if err != nil {
			return nil, err
		}

		var categories []string
		for _, category := range v.Categories {
			categories = append(categories, category.Value)
		}

		authorID, err := uuid.FromString(v.UserId)
		if err != nil {
			return nil, err
		}

		result[i] = &ArticleResolver{
			meta: structure.Article{
				Id:        uid,
				Title:     v.Title,
				Slug:      v.Slug,
				Content:   v.Content,
				Status:    v.Status,
				CreatedAt: v.CreatedAt,
				UpdatedAt: v.UpdatedAt,
				Category:  categories,
				Thumbnail: v.Thumbnail,
				IdAuthor:  authorID,
			},
		}
	}
	return &result, nil
}

//DISINI
func (resolver *Resolver) GetArticleByUserID(context context.Context, args struct {
	IdAuthor graphql.ID
	Offset   int32
	Limit    int32
}) (*[]*ArticleResolver, error) {
	logrus.Println(">>>", args.IdAuthor)
	response, err := resolver.ArticleService.GetArticleByUserId(context, &pb.ArticleRequest{
		UserId: string(args.IdAuthor),
		Offset: args.Offset,
		Limit:  args.Limit,
	})

	logrus.Println(response)

	if err != nil {
		return nil, err
	}

	articles := response.ArticleList
	if err != nil {
		return nil, err
	}

	result := make([]*ArticleResolver, len(articles))

	for i, v := range articles {
		uid, err := uuid.FromString(v.Id)
		if err != nil {
			return nil, err
		}

		var categories []string
		for _, category := range v.Categories {
			categories = append(categories, category.Value)
		}

		authorID, err := uuid.FromString(v.UserId)
		if err != nil {
			return nil, err
		}

		result[i] = &ArticleResolver{
			meta: structure.Article{
				Id:        uid,
				Title:     v.Title,
				Slug:      v.Slug,
				Content:   v.Content,
				Status:    v.Status,
				CreatedAt: v.CreatedAt,
				UpdatedAt: v.UpdatedAt,
				Category:  categories,
				Thumbnail: v.Thumbnail,
				IdAuthor:  authorID,
			},
		}
	}
	return &result, nil
}

func (resolver *Resolver) GetArticles(context context.Context, args struct {
	Category *string
	Status   *string
	UserId   *graphql.ID
	Username *string
	Keyword  *string
	Offset   int32
	Limit    int32
}) (*[]*ArticleResolver, error) {

	if args.Category != nil {
		type categoryArgs struct {
			Category string
			Offset   int32
			Limit    int32
		}
		resolves, err := resolver.GetArticleByCategory(context, categoryArgs{*args.Category, args.Offset, args.Limit})
		if err != nil {
			return nil, err
		}

		logrus.Println(resolves)

		if args.Status != nil {
			var tempResolves []*ArticleResolver
			resolvesCopy := *resolves
			for idx, r := range resolvesCopy {
				if r.meta.Status == *args.Status {
					tempResolves = append(tempResolves, resolvesCopy[idx])
				}
			}

			resolves = &tempResolves
		}

		logrus.Println(resolves)

		if args.Username != nil {
			var tempResolves []*ArticleResolver
			resolvesCopy := *resolves
			type userArgs struct{ Username string }
			user, err := ServiceConnection.GetUserByUsername(context, userArgs{
				Username: *args.Username,
			})
			if err != nil {
				return nil, err
			}
			for idx, r := range resolvesCopy {
				if r.meta.IdAuthor.String() == user.meta.Id.String() {
					tempResolves = append(tempResolves, resolvesCopy[idx])
				}
			}

			resolves = &tempResolves
		}

		logrus.Println(resolves)

		if args.UserId != nil {
			var tempResolves []*ArticleResolver
			resolvesCopy := *resolves
			for idx, r := range resolvesCopy {
				uuid, err := uuid.FromString(string(*args.UserId))
				if err != nil {
					return nil, err
				}
				if r.author.Id == uuid {
					tempResolves = append(tempResolves, resolvesCopy[idx])
				}
			}
			resolves = &tempResolves
		}

		logrus.Println(resolves)

		if args.Keyword != nil {
			resolves = resolver.SearchArticle(*args.Keyword, resolves)
		}

		logrus.Println(resolves)

		return resolves, nil
	}

	if args.Status != nil {
		type statusArgs struct {
			Status string
			Limit  int32
			Offset int32
		}
		resolves, err := resolver.GetArticleByStatus(context, statusArgs{
			Status: *args.Status,
			Limit:  args.Limit,
			Offset: args.Offset,
		})
		if err != nil {
			return nil, err
		}
		if args.Username != nil {
			var tempResolves []*ArticleResolver
			resolvesCopy := *resolves
			type userArgs struct{ Username string }
			user, err := ServiceConnection.GetUserByUsername(context, userArgs{
				Username: *args.Username,
			})
			if err != nil {
				return nil, err
			}
			for idx, r := range resolvesCopy {
				if r.meta.IdAuthor.String() == user.meta.Id.String() {
					tempResolves = append(tempResolves, resolvesCopy[idx])
				}
			}

			resolves = &tempResolves
		}

		if args.UserId != nil {
			var tempResolves []*ArticleResolver
			resolvesCopy := *resolves
			for idx, r := range resolvesCopy {
				uuid, err := uuid.FromString(string(*args.UserId))
				if err != nil {
					return nil, err
				}
				if r.author.Id == uuid {
					tempResolves = append(tempResolves, resolvesCopy[idx])
				}
			}

			resolves = &tempResolves
		}

		if args.Keyword != nil {
			resolves = resolver.SearchArticle(*args.Keyword, resolves)
		}

		return resolves, nil
	}

	if args.Username != nil {
		type usernameArgs struct {
			Username string
			Offset   int32
			Limit    int32
		}
		resolves, err := resolver.GetArticleByUsername(context, usernameArgs{
			*args.Username,
			args.Offset,
			args.Limit,
		})
		if err != nil {
			return nil, err
		}

		if args.Keyword != nil {
			resolves = resolver.SearchArticle(*args.Keyword, resolves)
		}

		return resolves, nil
	}

	if args.UserId != nil {
		type userIdArgs struct {
			IdAuthor graphql.ID
			Offset   int32
			Limit    int32
		}
		resolves, err := resolver.GetArticleByUserID(context, userIdArgs{*args.UserId, args.Offset, args.Limit})
		if err != nil {
			return nil, err
		}

		if args.Keyword != nil {
			resolves = resolver.SearchArticle(*args.Keyword, resolves)
		}

		return resolves, nil
	}

	type searchArgs struct {
		Keyword string
		Offset  int32
		Limit   int32
	}
	if args.Keyword != nil {

		resolves, err := resolver.Search(context, searchArgs{*args.Keyword, args.Offset, args.Limit})
		if err != nil {
			return nil, err
		}

		return resolves, nil
	}

	type allArgs struct {
		Offset int32
		Limit  int32
	}
	return resolver.GetAllArticle(context, allArgs{args.Offset, args.Limit})
}

func (resolver *Resolver) GetArticleByUsername(context context.Context, args struct {
	Username string
	Offset   int32
	Limit    int32
}) (*[]*ArticleResolver, error) {
	userResponse, err := resolver.UserService.GetUserByUsername(context, &pb.UserRequest{Username: args.Username})
	if err != nil {
		return nil, err
	}

	type idUserArgs struct {
		IdAuthor graphql.ID
		Offset   int32
		Limit    int32
	}
	if err != nil {
		return nil, err
	}

	logrus.Println(">>>>", userResponse)

	response, err := resolver.GetArticleByUserID(context, idUserArgs{
		graphql.ID(userResponse.Id),
		args.Offset,
		args.Limit,
	})

	logrus.Println(response)

	if err != nil {
		return nil, err
	}

	return response, nil
}

func (resolver *Resolver) Search(context context.Context, args struct {
	Keyword string
	Offset  int32
	Limit   int32
}) (*[]*ArticleResolver, error) {
	response, err := resolver.ArticleService.Search(context, &pb.Keyword{
		Keyword: args.Keyword,
		Offset:  args.Offset,
		Limit:   args.Limit,
	})

	logrus.Println(response)

	if err != nil {
		return nil, err
	}

	articles := response.ArticleList
	if err != nil {
		return nil, err
	}

	result := make([]*ArticleResolver, len(articles))

	for i, v := range articles {
		uid, err := uuid.FromString(v.Id)
		if err != nil {
			return nil, err
		}

		var categories []string
		for _, category := range v.Categories {
			categories = append(categories, category.Value)
		}

		authorID, err := uuid.FromString(v.UserId)
		if err != nil {
			return nil, err
		}

		result[i] = &ArticleResolver{
			meta: structure.Article{
				Id:        uid,
				Title:     v.Title,
				Slug:      v.Slug,
				Content:   v.Content,
				Status:    v.Status,
				CreatedAt: v.CreatedAt,
				UpdatedAt: v.UpdatedAt,
				Category:  categories,
				Thumbnail: v.Thumbnail,
				IdAuthor:  authorID,
			},
		}
	}
	return &result, nil
}

func (resolver *Resolver) SearchArticle(Keyword string, Articles *[]*ArticleResolver) *[]*ArticleResolver {

	var tempResolves []*ArticleResolver
	ArticlesCopy := *Articles
	for i, r := range ArticlesCopy {
		if strings.Contains(strings.ToLower(r.meta.Title), strings.ToLower(Keyword)) {
			tempResolves = append(tempResolves, ArticlesCopy[i])
		}
	}

	return &tempResolves
}

func (resolver *Resolver) GetRelatedArticles(context context.Context, args struct{ Slug string }) (*[]*ArticleResolver, error) {
	type slugArgs struct{ Slug string }
	resolve, err := resolver.GetArticleBySlug(context, slugArgs{args.Slug})
	if err != nil {
		return nil, err
	}

	var result []*ArticleResolver
	type categoryArgs struct {
		Category string
		Offset   int32
		Limit    int32
	}
	for _, c := range resolve.meta.Category {
		categoryResolves, err := resolver.GetArticleByCategory(context, categoryArgs{c, 0, 4})
		if err != nil {
			return nil, err
		}

		categoryResolvesValue := *categoryResolves
		for i, categoryResolve := range categoryResolvesValue {
			if categoryResolve.meta.Slug != args.Slug && categoryResolve.meta.Status == "PUBLISHED" {
				result = append(result, categoryResolvesValue[i])
			}

			if len(result) == 3 {
				break
			}
		}

		if len(result) == 3 {
			break
		}
	}

	return &result, nil
}

func (resolver *Resolver) GetCategoriesByUserID(context context.Context, args struct {
	IdAuthor graphql.ID
	Limit    int32
	Offset   int32
}) (*[]*ArticleResolver, error) {
	defaultCategory := []string{"NEWS", "POLITIK", "OTOMOTIF", "ENTERTAINMENT"}
	defaultCategorySlug := []string{"news", "politik", "otomotif", "entertainment"}

	authorUid, err := uuid.FromString(string(args.IdAuthor))
	if err != nil {
		return nil, err
	}

	// result := []*ArticleResolver{}
	result := make([]*ArticleResolver, args.Limit)

	var i, idx, totalDefaultCategory int32
	LimitCustomCategory := args.Limit

	if args.Offset < 4 {
		for i = 0; i < args.Limit; i++ {
			if i+args.Offset < 4 {
				result[i] = &ArticleResolver{
					meta: structure.Article{
						IdAuthor: authorUid,
					},
					category: structure.Category{
						Name:         defaultCategory[i+args.Offset],
						SlugCategory: defaultCategorySlug[i+args.Offset],
					},
				}
				totalDefaultCategory++
			} else {
				LimitCustomCategory = args.Limit - i
				break
			}
		}
	}

	var inputOffset int32
	if args.Offset > 0 {
		inputOffset = args.Offset - 4
	} else {
		inputOffset = args.Offset
	}
	response, err := resolver.ArticleService.GetCategoryByUserId(context, &pb.Category{
		UserId: string(args.IdAuthor),
		Limit:  LimitCustomCategory,
		Offset: inputOffset,
	})
	if err != nil {
		return nil, err
	}

	categories := response.CategoryList
	if err != nil {
		return nil, err
	}

	if int32(len(categories)) < LimitCustomCategory {
		result = result[:int32(len(categories))+totalDefaultCategory]
		for idx = 0; idx < int32(len(categories)); idx++ {
			result[idx+args.Limit-LimitCustomCategory] = &ArticleResolver{
				meta: structure.Article{
					IdAuthor: authorUid,
				},
				category: structure.Category{
					Name:         categories[idx].Value,
					SlugCategory: categories[idx].Slug,
				},
			}
		}
		return &result, nil
	}

	for idx = 0; idx < LimitCustomCategory; idx++ {
		result[idx+args.Limit-LimitCustomCategory] = &ArticleResolver{
			meta: structure.Article{
				IdAuthor: authorUid,
			},
			category: structure.Category{
				Name:         categories[idx].Value,
				SlugCategory: categories[idx].Slug,
			},
		}
	}
	return &result, nil
}

func (resolver *Resolver) GetCategoriesBySlug(context context.Context, args struct{ Slug string }) (*[]*ArticleResolver, error) {
	response, err := resolver.ArticleService.GetCategoryBySlug(context, &pb.Category{
		Slug: string(args.Slug),
	})
	if err != nil {
		return nil, err
	}

	categories := response.CategoryList
	if err != nil {
		return nil, err
	}

	result := make([]*ArticleResolver, len(categories)+4)

	var authorUid uuid.UUID
	for i, v := range categories {
		if i == 0 {
			authorUid, err = uuid.FromString(v.UserId)
			if err != nil {
				return nil, err
			}
		}
		result[i+4] = &ArticleResolver{
			meta: structure.Article{
				IdAuthor: authorUid,
			},
			category: structure.Category{
				Name:         v.Value,
				SlugCategory: v.Slug,
			},
		}
	}

	defaultCategory := []string{"NEWS", "POLITIK", "OTOMOTIF", "ENTERTAINMENT"}
	defaultCategorySlug := []string{"news", "politik", "otomotif", "entertainment"}
	for idx, c := range defaultCategory {
		result[idx] = &ArticleResolver{
			meta: structure.Article{
				IdAuthor: authorUid,
			},
			category: structure.Category{
				Name:         c,
				SlugCategory: defaultCategorySlug[idx],
			},
		}
	}
	return &result, nil
}
