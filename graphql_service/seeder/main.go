package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"
)

const url = "http://167.71.202.17/graphql"
const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NjU0OTYwNjksInVzZXIiOiJhZjMwMjgwNC0wZDJiLTQ4MDUtODUyNi00ZDA5MWVkNmVlMzgifQ.UhM5yFcRwYyW-hdSqI92PEt7vKvMlhRHZOJzD4VsBxQ"
const articleContent = `wdahlkhwdli`

func main() {
	client := http.Client{
		Timeout: time.Duration(10 * time.Second),
	}

	// for i := 0; i < 1000; i++ {
	// 	requestBody, err := json.Marshal(map[string]string{
	// 		"query": "mutation{\n  CreateArticle(article:{\n    title:\"lorem ipsum\",\n    slug:\"\",\n    content:\"" + articleContent + "\",\n    status:\"PUBLISHED\",\n    category:[\"NEWS\"]\n    thumbnail:\"\"\n  }) {\n    id\n  }\n}",
	// 	})

	// 	if err != nil {
	// 		log.Fatal(err)
	// 	}

	// 	request, err := http.NewRequest("POST", url, bytes.NewBuffer(requestBody))
	// 	request.Header.Set("Authorization", token)

	// 	if err != nil {
	// 		log.Fatal(err)
	// 	}

	// 	fmt.Println(i)
	// 	_, err = client.Do(request)
	// 	if err != nil {
	// 		log.Fatal(err)
	// 	}
	// }

	for i := 0; i < 1000; i++ {
		requestBody, err := json.Marshal(map[string]string{
			"query": "mutation {\n  CreateComment(comment:{\n    content:\"klahudkahdw\",\n    idArticle:\"7fca595e-a03c-435f-a200-80041fb7a224\"\n  }){\n    id\n  }\n}",
		})

		if err != nil {
			log.Fatal(err)
		}

		request, err := http.NewRequest("POST", url, bytes.NewBuffer(requestBody))
		request.Header.Set("Authorization", token)

		if err != nil {
			log.Fatal(err)
		}

		fmt.Println(i)
		_, err = client.Do(request)
		if err != nil {
			log.Fatal(err)
		}
	}
	fmt.Scanln()
}
