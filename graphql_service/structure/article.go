package structure

import (
	uuid "github.com/satori/uuid"
)

type Article struct {
	Id        uuid.UUID
	Title     string
	CreatedAt string
	UpdatedAt string
	Slug      string
	Content   string
	Status    string
	Category  []string
	IdAuthor  uuid.UUID
	Thumbnail string
	// Author    User
}

type Category struct {
	Name         string
	SlugCategory string
}
