package structure

import uuid "github.com/satori/uuid"

type Comment struct {
	Id        uuid.UUID
	Content   string
	IdAuthor  uuid.UUID
	IdArticle uuid.UUID
}
