package structure

import "github.com/satori/uuid"

type User struct {
	Id       uuid.UUID
	Username string
	Email    string
	Fullname string
	Password string
}
