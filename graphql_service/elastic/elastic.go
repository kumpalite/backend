package elastic

import (
	"context"
	"encoding/json"
	"graphql_service/structure"

	"github.com/olivere/elastic"
)

const (
	elasticIndexName = "articles"
	elasticTypeName  = "article"
)

type Elastic struct {
	elasticClient *elastic.Client
}

func New() (*Elastic, error) {
	var err error
	elasticClient, err := elastic.NewClient(
		elastic.SetURL("http://elasticsearch:9200"),
		elastic.SetSniff(false),
	)

	if err != nil {
		return nil, err
	}

	return &Elastic{
		elasticClient,
	}, nil
}

func (es *Elastic) Insert(article structure.Article) error {
	bulk := es.elasticClient.Bulk().Index(elasticIndexName).Type(elasticTypeName)

	bulk.Add(elastic.NewBulkIndexRequest().Id(article.Id.String()).Doc(article))

	if _, err := bulk.Do(context.Background()); err != nil {
		return err
	}

	return nil
}

func (es *Elastic) Search(keyword string) ([]structure.Article, error) {
	esQuery := elastic.NewMultiMatchQuery(keyword, "title", "content").
		Fuzziness("2").
		MinimumShouldMatch("2")
	result, err := es.elasticClient.Search().
		Index(elasticIndexName).
		Query(esQuery).
		From(5).Size(5).
		Do(context.Background())

	var articles []structure.Article
	for _, hit := range result.Hits.Hits {
		var article structure.Article
		json.Unmarshal(*hit.Source, &article)
		articles = append(articles, article)
	}

	if err != nil {
		return nil, err
	}

	return articles, nil
}
