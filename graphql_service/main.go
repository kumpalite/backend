package main

import (
	"graphql_service/config"
	"graphql_service/handler"
	"os"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func main() {
	args := os.Args[1:]

	var mode string
	if len(args) != 0 {
		mode = args[0]
	} else {
		mode = "prod"
	}

	cfg := config.Load(mode)
	handler := handler.New(&cfg)
	e := echo.New()

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
	}))
	e.POST("/login", handler.Login)
	e.POST("/register", handler.Register)

	//graphql := e.Group("graphql")
	//graphql.Use(middleware.CORSWithConfig(middleware.CORSConfig{
	//	AllowOrigins: []string{"*"},
	//	AllowHeaders: []string{echo.HeaderAuthorization},
	//	//echo.HeaderContentType, echo.HeaderAccept},
	//}))
	// echo.POST("/", handler.Query) // Graphql query
	// graphql.GET("/", handler.Playground) // Graphql playground

	e.POST("/graphql", handler.Query)
	e.GET("/graphql", handler.Playground)
	e.Logger.Fatal(e.Start(cfg.PORT))
}
