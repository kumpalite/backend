package auth

import (
	"context"
	pb "graphql_service/proto"
	"net/http"
	"time"

	"graphql_service/config"
	service "graphql_service/grpc"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"github.com/sirupsen/logrus"
)

type AuthHandler struct {
	Config *config.Config
}

// register new user
func (handler *AuthHandler) Register(c echo.Context) error {

	// get user rquest
	request := new(pb.UserRequest)
	if err := c.Bind(request); err != nil {
		logrus.Error(err)
		return err
	}

	// connect to grpc
	userServiceConnection, err := service.NewUserServiceConnector(*handler.Config)

	if err != nil {
		logrus.Error(err)
		logrus.Warn(handler.Config)
		echo.NewHTTPError(http.StatusInternalServerError, err)
	}

	// create new user
	response, err := userServiceConnection.CreateUser(context.Background(), request)

	if err != nil {
		logrus.Error(err)
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}

	token := jwt.New(jwt.SigningMethodHS256)

	// Set claims
	claims := token.Claims.(jwt.MapClaims)
	claims["user"] = response.Id
	claims["exp"] = time.Now().Add(time.Hour * 72).Unix()

	// Generate encoded token and send it as response.
	t, err := token.SignedString([]byte("secret"))

	if err != nil {
		logrus.Println(err)
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}

	// returning token
	c.JSON(http.StatusOK, echo.Map{
		"token": t,
	})
	return nil
}

// authenticate is being used as login and jwt creation
func (handler *AuthHandler) Login(c echo.Context) error {

	// get identifier and password from JSON
	request := new(pb.AuthRequest)
	if err := c.Bind(request); err != nil {
		logrus.Error(request)
		return err
	}

	// connect to grpc
	userServiceConnection, err := service.NewUserServiceConnector(*handler.Config)

	if err != nil {
		logrus.Error(err)
		logrus.Warn(handler.Config)
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}

	// validate if user exist
	response, err := userServiceConnection.Authenticate(context.Background(), request)

	if err != nil {
		logrus.Error(err)
		var status int
		if err.Error() == "rpc error: code = Unknown desc = User not found" {
			status = http.StatusNotFound
		} else {
			status = http.StatusUnauthorized
		}
		return echo.NewHTTPError(status, err)
	}

	token := jwt.New(jwt.SigningMethodHS256)

	// Set claims
	claims := token.Claims.(jwt.MapClaims)
	claims["user"] = response.Id
	claims["exp"] = time.Now().Add(time.Hour * 72).Unix()

	// Generate encoded token and send it as response.
	t, err := token.SignedString([]byte("secret"))

	// returning token
	c.JSON(http.StatusOK, echo.Map{
		"token": t,
	})

	return nil
}
