package handler

import (
	"graphql_service/config"
	"graphql_service/handler/auth"
	"graphql_service/handler/graphql"
)

type Handler struct {
	auth.AuthHandler
	graphql.GraphQLHandler
}

func New(config *config.Config) Handler {
	h := Handler{}

	// inject config if needed
	h.AuthHandler.Config = config
	h.GraphQLHandler.Config = config
	return h
}
