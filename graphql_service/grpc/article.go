package grpc

import (
	"graphql_service/config"
	pb "graphql_service/proto"

	"github.com/pkg/errors"
	"google.golang.org/grpc"
)

func NewArticleServiceConnector(cfg config.Config) (pb.ArticleServiceClient, error) {
	var conn *grpc.ClientConn
	var err error

	conn, err = grpc.Dial(cfg.GrpcArticleAddr, grpc.WithInsecure())

	if err != nil {
		return nil, errors.Wrap(err, "failed to dial")
	}

	client := pb.NewArticleServiceClient(conn)
	return client, nil
}
