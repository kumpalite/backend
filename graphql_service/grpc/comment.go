package grpc

import (
	"graphql_service/config"
	pb "graphql_service/proto"

	"github.com/pkg/errors"
	"google.golang.org/grpc"
)

func NewCommentServiceConnector(cfg config.Config) (pb.CommentServiceClient, error) {
	var conn *grpc.ClientConn
	var err error

	conn, err = grpc.Dial(cfg.GrpcCommentAddr, grpc.WithInsecure())

	if err != nil {
		return nil, errors.Wrap(err, "failed to dial")
	}

	client := pb.NewCommentServiceClient(conn)
	return client, nil
}
