package grpc

import (
	"graphql_service/config"
	pb "graphql_service/proto"

	"github.com/pkg/errors"
	"google.golang.org/grpc"
)

func NewUserServiceConnector(cfg config.Config) (pb.UserServiceClient, error) {
	var conn *grpc.ClientConn
	var err error

	conn, err = grpc.Dial(cfg.GrpcUserAddr, grpc.WithInsecure())

	if err != nil {
		return nil, errors.Wrap(err, "failed to dial")
	}

	client := pb.NewUserServiceClient(conn)
	return client, nil
}
