package config

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

type Config struct {
	PORT            string
	GrpcUserAddr    string
	GrpcArticleAddr string
	GrpcCommentAddr string
}

func Load(mode string) Config {
	if mode == "dev" {
		err := godotenv.Load(".env")
		if err != nil {
			log.Println(err)
		}
	}

	PORT := os.Getenv("PORT")
	GrpcUserAddr := os.Getenv("USER_SERVICE_ADDR")
	GrpcArticleAddr := os.Getenv("ARTICLE_SERVICE_ADDR")
	GrpcCommentAddr := os.Getenv("COMMENT_SERVICE_ADDR")

	if PORT == "" {
		PORT = ":3000"
	}

	if GrpcUserAddr == "" {
		GrpcUserAddr = ":9000"
	}

	return Config{
		PORT:            PORT,
		GrpcUserAddr:    GrpcUserAddr,
		GrpcArticleAddr: GrpcArticleAddr,
		GrpcCommentAddr: GrpcCommentAddr,
	}
}
